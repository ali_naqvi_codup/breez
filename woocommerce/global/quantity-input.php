<?php
/**
 * Product quantity inputs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/quantity-input.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.0.0
 */
$count=0;
defined( 'ABSPATH' ) || exit;

if ( $max_value && $min_value === $max_value ) {
	?>
	<div class="quantity hidden">
		<input type="hidden" id="<?php echo esc_attr( $input_id ); ?>" class="qty" name="<?php echo esc_attr( $input_name ); ?>" value="<?php echo esc_attr( $min_value ); ?>" />
	</div>
	<?php
} else {
	if(isset($_GET['total']))
	{
		$this->cart->set_cart_contents_total($_GET['total']);
	}
	/* translators: %s: Quantity. */
	$label = ! empty( $args['product_name'] ) ? sprintf( esc_html__( '%s quantity', 'woocommerce' ), wp_strip_all_tags( $args['product_name'] ) ) : esc_html__( 'Quantity', 'woocommerce' );
	?>
	<?php do_action( 'woocommerce_before_quantity_input_field' ); ?>
	<label class="screen-reader-text" for="<?php echo esc_attr( $input_id ); ?>"><?php echo esc_attr( $label ); ?></label>

	<?php
								echo apply_filters(
									'woocommerce_cart_item_remove_link',
									sprintf(
										'<a href="%s" class="bin-icon remove" aria-label="%s" data-product_id="%s" data-product_sku="%s" id="bin_'.$input_id.'">
										<img src="https://dev-dtcbreez.codupcloud.com/wp-content/themes/theme-breeze/public/images/_bin-icon.png">
										</a>',
										esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
										esc_html__( 'Remove this item', 'woocommerce' ),
										esc_attr( $product_id ),
										esc_attr( $_product->get_sku() )
									),
									$cart_item_key
								);
								?>
								<input class="minus hidden down" onclick="minus(this)" id='minus_<?php echo $input_id; ?>' type="button" data-id="<?php echo $input_id; ?>" data-quantity="<?php echo $cart_item['quantity'];?>" data-item_id="<?php echo $product_id;?>" value="-">
								<input
								type="number"
								id="<?php echo $input_id; ?>"
								class="<?php echo esc_attr( join( ' ', (array) $classes ) ); ?>"
								step="<?php echo esc_attr( $step ); ?>"
								min="<?php echo esc_attr( $min_value ); ?>"
								max="<?php echo esc_attr( 0 < $max_value ? $max_value : '' ); ?>"
								name="<?php echo esc_attr( $input_name ); ?>"
								value="<?php echo esc_attr( $input_value ); ?>"
								title="<?php echo esc_attr_x( 'Qty', 'Product quantity input tooltip', 'woocommerce' ); ?>"
								size="4"
								placeholder="<?php echo esc_attr( $placeholder ); ?>"
								inputmode="<?php echo esc_attr( $inputmode ); ?>"
								autocomplete="<?php echo esc_attr( isset( $autocomplete ) ? $autocomplete : 'on' ); ?>"
								/>
								<input class="plus up" type="button" onclick="plus(this)" id='plus_<?php echo $input_id; ?>' data-id="<?php echo $input_id;?>"  data-quantity="<?php echo $cart_item['quantity'];?>" data-item_id="<?php echo $product_id;?>" value="+">
								<?php do_action( 'woocommerce_after_quantity_input_field' ); ?>
								<?php
							}
							?>