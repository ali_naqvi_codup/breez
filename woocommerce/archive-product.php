<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header();

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */

?>
<section x-data class="products-hero">
	<div class="container">
		<div class="products-hero__inner">
			<div class="cols-container items-center md:px-1-cols lg:px-0">
				<article class="w-6-cols md:w-8-cols lg:w-5-cols">
					<h1>Our Products</h1>
					<p class="max-w-md lg:pb-0">
						All our products are made using the highest quality full-spectrum
						extracts to preserve the full natural goodness of the cannabis
						plant. Take your pick.
					</p>

					<!-- Filter button -->
					<button
					class="fake-input-btn lg:hidden"
					@click="$store.global.toggleFilterModal()"
					>
					Filter by

					<div class="icon">
						<div>
							<include src="partials/svg/cart/_add-icon.svg"></include>
						</div>
					</div>
				</button>
			</article>

			<article class="hidden lg:block w-7-cols">
				<div class="products-hero__image">
					<img src="<?php echo get_template_directory_uri(); ?>/public/images/contact/lifestyle-contact.jpg" alt=""/>
				</div>
			</article>
		</div>
	</div>
</div>
</section>

<div class="container lg:py-10">
	<section class="c-product-filter hidden lg:block">
		<div class="c-product-filter__inner">
			<div class="">
				<p class="bp-0 f-body">Shop by:</p>
			</div>

			<form action="" class="flex space-x-8">
				<!-- Flavours dropdown -->        

				<div class="w-4-cols">
					<div class="cselect">
					    <?php
                            // $terms = get_terms('Flavor');
                            // echo '<select class="c-single-filter"><i class="fas fa-angle-up"></i>';
                            // echo '<option selected="true" disabled="disabled">Flavor</option>>';
                            // foreach ( $terms as $term) {
                            //     echo '<option value="'.$term->slug.'">'.$term->name.'</option>';
                            // }
                            	$prodfilter = get_option("prodfilter");
								 $optionarr = json_decode($prodfilter);
									print_r($prodfilter);
						echo 'here';

								foreach($optionarr as $allopt) 
								echo do_shortcode('[facetwp facet="'.$allopt.'"]');
//                             echo "</select>";
                          ?>
						<!--<select class="c-single-filter">-->
						<!--	<option>Flavor</option>-->
						<!--	<option>Berry Daytime</option>-->
						<!--	<option>Berry Nighttime</option>-->
						<!--	<option>Cinnamon</option>-->
						<!--	<option>Citrus</option>-->
						<!--	<option>Mint</option>-->
						<!--	<option>Unflavored</option>-->
						<!--</select>-->
					</div>
				</div>

				<div class="w-4-cols">
					<div class="cselect">
					     <?php
                            $terms = get_terms('Effects');
                            echo '<select class="c-single-filter"><i class="fas fa-angle-up"></i>';
                            echo '<option selected="true" disabled="disabled">Effect</option>>';
                            foreach ( $terms as $term) {
                                echo '<option value="'.$term->slug.'">'.$term->name.'</option>';
                            }
                            echo "</select>";
                          ?>
						<!--<select class="c-single-filter"><i class="fas fa-angle-up"></i>-->
						<!--	<option>Effect</option>-->
						<!--	<option>Sativa</option>-->
						<!--	<option>Hybrid</option>-->
						<!--	<option>Indica</option>-->
						<!--	<option>Microdose</option>-->
						<!--	<option>Sleep</option>-->
						<!--	<option>Relief</option>-->
						<!--</select>-->
					</div>
				</div>

				<div class="w-4-cols">
					<div class="cselect">
						 <?php
                            $terms = get_terms('Dose');
                            echo '<select class="c-single-filter"><i class="fas fa-angle-up"></i>';
                            echo '<option selected="true" disabled="disabled">Dose</option>>';
                            foreach ( $terms as $term) {
                                echo '<option value="'.$term->slug.'">'.$term->name.'</option>';
                            }
                            echo "</select>";
                          ?>
						<!--<select class="c-single-filter"><i class="fas fa-angle-up"></i>-->
						<!--	<option>Dose</option>-->
						<!--	<option>High THC</option>-->
						<!--	<option>Low THC</option>-->
						<!--	<option>CBD</option>-->
						<!--</select>-->
					</div>
				</div>
			</form>
		</div>
	</section>
</div>

<?php if ( woocommerce_product_loop() ) { 

	woocommerce_product_loop_start(); ?>

	<!-- Start Mints Products Area -->
	<section class="projects-section c-products-carousel overflow-hidden owl-carousel">
		<div class="container">
			<div class="c-products-carousel__header">
				<a href="#" class="c-products-carousel__title"> Mints </a>

				<article class="c-products-carousel__icon">
					<img src="<?php echo get_template_directory_uri(); ?>/public/images/_mint-icon.png" />
				</article>
			</div>
		</div>
		<div id="projects-carousel" class="projects-carousel box-shadow owl-carousel">

			<?php
			$args = array( 'post_type' => 'product', 'posts_per_page' => 8, 'product_cat' => 'mints', 'orderby' => 'rand' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>

				<div class="project-item">
					<div class="post-slide">
						<div class="">
							<article>
								<a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>" class="product-image" style="--aspect-ratio: (1 / 1)">

									<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );?>
									<img src="<?php  echo $image[0]; ?>" data-id="<?php echo $loop->post->ID; ?>">
								</a>
							</article>

							<article class="product-detail">
								<h4 class="mints-name"><?php the_title(); ?></h4>
								<!-- <a href="" class="product__type">
									<span x-text="mints.tag"><?php echo single_term_title();?></span>
								</a> -->
								<?php $categ = $product->get_categories();
								echo $categ;
								?>

								<p class="mints-price">
									<?php $price = get_post_meta( get_the_ID(), '_regular_price', true); ?>
									<?php echo get_woocommerce_currency_symbol() . $price; ?>
								</p>

								<div>
									<span class="c-product-tag" x-text="mints.type">
										<?php echo wc_get_product_tag_list( $product->get_id(), ', ' ); ?>
									</span>
								</div>
							</article>

							<!-- Add to cart button  -->
							<a href="<?php echo get_site_url()."/cart/?add-to-cart=".esc_attr($loop->post->ID); ?>" class="btn-primary add-to-cart">
								Add to Cart
							</a>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
			<?php wp_reset_query(); ?>


		</div>
	</div>
</section>
<?php
woocommerce_product_loop_end();


} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}
?>
<!-- End Mints Products Area -->

<!-- Start Tincture Sprays Area -->
<section class="tincture-sprays-section c-products-carousel overflow-hidden owl-carousel">
	<div class="container">
		<div class="c-products-carousel__header">
			<a href="#" class="c-products-carousel__title"> Tincture Sprays </a>

			<article class="c-products-carousel__icon">
				<img src="<?php echo get_template_directory_uri(); ?>/public/images/_spray-icon.png" />
			</article>
		</div>
	</div>
	<div id="tincture-sprays-carousel" class="tincture-sprays-carousel box-shadow owl-carousel">
		
		<?php
		$args = array( 'post_type' => 'product', 'posts_per_page' => 8, 'product_cat' => 'spray', 'orderby' => 'rand' );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>

			<div class="project-item">
				<div class="post-slide">
					<div class="">
						<article>
							<a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>" class="product-image" style="--aspect-ratio: (1 / 1)">

								<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );?>
								<img src="<?php  echo $image[0]; ?>" data-id="<?php echo $loop->post->ID; ?>">
							</a>
						</article>

						<article class="product-detail">
							<h4 class="mints-name"><?php the_title(); ?></h4>
								<!-- <a href="" class="product__type">
									<span x-text="mints.tag"><?php echo single_term_title();?></span>
								</a> -->
								<?php $categ = $product->get_categories();
								echo $categ;
								?>

								<p class="mints-price">
									<?php $price = get_post_meta( get_the_ID(), '_regular_price', true); ?>
									<?php echo get_woocommerce_currency_symbol() . $price; ?>
								</p>

								<div>
									<span class="c-product-tag" x-text="mints.type">
										<?php echo wc_get_product_tag_list( $product->get_id(), ', ' ); ?>
									</span>
								</div>
							</article>

							<!-- Add to cart button  -->
							<a href="<?php echo get_site_url()."/cart/?add-to-cart=".esc_attr($loop->post->ID); ?>" class="btn-primary add-to-cart">
								Add to Cart
							</a>
						</div>
					</div>
				</div>

			<?php endwhile; ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>
</section>
<!-- End Tincture Sprays Products Area -->

<!-- Start Tablets Area -->
<section class="tablets-section c-products-carousel overflow-hidden owl-carousel">
	<div class="container">
		<div class="c-products-carousel__header">
			<a href="#" class="c-products-carousel__title"> Tablets </a>

			<article class="c-products-carousel__icon">
				<img src="<?php echo get_template_directory_uri(); ?>/public/images/_tablet-icon.png" />
			</article>
		</div>
	</div>
	<div id="tablets-carousel" class="tincture-sprays-carousel box-shadow owl-carousel">

		<?php
		$args = array( 'post_type' => 'product', 'posts_per_page' => 8, 'product_cat' => 'spray', 'orderby' => 'rand' );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>

			<div class="project-item">
				<div class="post-slide">
					<div class="">
						<article>
							<a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>" class="product-image" style="--aspect-ratio: (1 / 1)">

								<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );?>
								<img src="<?php  echo $image[0]; ?>" data-id="<?php echo $loop->post->ID; ?>">
							</a>
						</article>

						<article class="product-detail">
							<h4 class="mints-name"><?php the_title(); ?></h4>
								<!-- <a href="" class="product__type">
									<span x-text="mints.tag"><?php echo single_term_title();?></span>
								</a> -->
								<?php $categ = $product->get_categories();
								echo $categ;
								?>

								<p class="mints-price">
									<?php $price = get_post_meta( get_the_ID(), '_regular_price', true); ?>
									<?php echo get_woocommerce_currency_symbol() . $price; ?>
								</p>

								<div>
									<span class="c-product-tag" x-text="mints.type">
										<?php echo wc_get_product_tag_list( $product->get_id(), ', ' ); ?>
									</span>
								</div>
							</article>

							<!-- Add to cart button  -->
							<a href="<?php echo get_site_url()."/cart/?add-to-cart=".esc_attr($loop->post->ID); ?>" class="btn-primary add-to-cart">
								Add to Cart
							</a>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
			<?php wp_reset_query(); ?>

		</div>
	</div>
</section>
<!-- End Tincture Sprays Products Area -->

<?php
get_footer();
