<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header();

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */

?>
<div class="c-secondary-nav">
	<div class="container">
		<nav>
			<ul>
				<?php wp_list_categories( array('taxonomy' => 'product_cat', 'title_li'  => '') );?>
			</ul>
		</nav>
	</div>
</div>
<?php
if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	woocommerce_product_loop_start();
	$check = true;

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			
			if($check){

				?>

				<!-- Hero section -->
				<section x-data class="cat-hero">
					<div class="container">
						<div class="cat-hero__inner">
							<div class="cols-container md:px-1-cols lg:px-0">
								<article class="cat-hero__title lg:w-4-cols">
									<h1><?php echo single_term_title();?></h1>
								</article>

								<article class="cat-hero__intro lg:w-4-cols">
									<p>
										<?php global $post, $product;
										$categ = $product->get_categories();
										$term = get_term_by ( 'name' , strip_tags($categ), 'product_cat' );
										echo $term->description; ?>
									</p>
								</article>

								<article class="cat-hero__icon">
									<?php echo wp_get_attachment_image( get_term_meta( get_queried_object_id(), 'thumbnail_id', 1 ), 'thumbnail' ); ?>
								</article>
							</div>

						</div>
					</div>
				</section>
				<section class="category-products">
					<div class="container">
						<div class="category-row">
							<?php 
							$check = false;
						}
						?>

						<!-- Category Grid -->
						<div class="w-3-cols mb-10 md:w-1/2-cols lg:w-3-cols products-grid">
							<article>
								<a href="<?php echo get_permalink( $loop->post->ID ) ?>" class="product-image" style="--aspect-ratio: (1 / 1)"
									>
									<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );?>

									<img src="<?php  echo $image[0]; ?>" data-id="<?php echo $loop->post->ID; ?>">
								</a>
							</article>

							<article class="product-detail">
								<h4 class="mints-name"><?php echo get_the_title(get_the_id());?></h4>

								<a href="" class="product__type">
									<span x-text="mints.tag"><?php echo single_term_title();?></span>
								</a>
								<p class="mints-price">
									<?php $price = get_post_meta( get_the_ID(), '_regular_price', true); ?>
									<?php echo get_woocommerce_currency_symbol() . $price; ?>
								</p>

								<div>
									<span class="c-product-tag" x-text="mints.type">
										<?php echo wc_get_product_tag_list( $product->get_id(), ', ' ); ?>
									</span>
								</div>
							</article>

							<!-- Add to cart button  -->
							<a href="<?php echo get_site_url()."/cart?add-to-cart=".esc_attr($loop->post->ID); ?>" class="btn-primary add-to-cart">
								Add to Cart
							</a>
						</div>

						<?php
					}
					?>
				</div>
			</div>
		</section>
		<?php
	}

	woocommerce_product_loop_end();


} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */


/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
get_footer();
