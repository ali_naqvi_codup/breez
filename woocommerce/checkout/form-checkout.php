<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<script>
	var ajaxurl = '<?= admin_url( 'admin-ajax.php' ) ?>';
</script>
<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>
		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>
		</div>
		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>


	<!-- Checkout nav -->
	<div class="c-checkout-nav">
		<div class="container">
			<div class="c-checkout-nav__inner">
				<a href="<?php echo wc_get_cart_url() ?>">
					<article class="flex items-center">
						<div class="mr-4">
							<img src="<?php echo get_template_directory_uri(); ?>/public/images/_nav-back-arrow.png">
						</div>

						<div>
							<h4>Back to cart</h4>
						</div>
					</article>
				</a>
			</div>
		</div>
	</div>


	<section class="container">
		<div class="cols-container">
			<div class="w-5-cols checkout-order-details">
				<section class="c-cart lg:block bg-shade-grey-100 sticky pr-1-cols pl-14 lg:py-20"
				x-data
				>
				<div class="container">
					<h3 id="order_review_heading"><?php esc_html_e( 'Order summary', 'woocommerce' ); ?></h3>

					<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>

					<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

					<div id="order_review" class="woocommerce-checkout-review-order">
						<?php do_action( 'woocommerce_checkout_order_review' ); ?>
					</div>

					<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>

					<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
				</div>
			</section>

		</div>
		<div class="lg:w-5-cols lg:ml-1-cols lg:py-20">			
			<div class="container space-y-5">
				<h3 class="hidden lg:block">Checkout</h3>
				<div class="flex justify-between mb-8 c-checkout-form">
					<h3 class="f-display font-normal">Personal Details</h3>
					<img src="<?php echo get_template_directory_uri(); ?>/public/images/_personal-details-icon.png">
				</div>

				<div class="woocommerce-billing-fields">

					<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

					<div class="woocommerce-billing-fields__field-wrapper">
						<?php
						$fields = $checkout->get_checkout_fields( 'billing' );

						foreach ( $fields as $key => $field ) {
							woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
						}
						?>
					</div>

					<?php do_action( 'woocommerce_after_checkout_billing_form', $checkout ); ?>
				</div>

				<div class="flex justify-between my-8 c-checkout-form">
					<h3 class="f-display font-normal">Extra</h3>
					<img src="<?php echo get_template_directory_uri(); ?>/public/images/_extra-icon.png">
				</div>

				<?php	
				do_action( 'woocommerce_before_checkout_form', $checkout );

					// If checkout registration is disabled and not logged in, the user cannot checkout.
				if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
					echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
					return;
				}
				?>
				<div class="col-2">
					<?php do_action( 'woocommerce_checkout_shipping' ); ?>
				</div>

			</div>
			<section class="c-payment-summary">
				<section class="pt-8">
					<div class="container">
						<h3 class="f-body-lg mb-4">Total Taxes & Fees</h3>
						<section
						class="bg-shade-grey-100 py-4 mb-4 breakout lg:breakout-reset lg:bg-white"
						>
						<div class="container">
							<div class="flex justify-between">
								<p class="f-display font-normal"><?php esc_html_e( 'Total', 'woocommerce' ); ?></p>
								<span class="total_qty"><p class="f-display font-normal"><?php wc_cart_totals_order_total_html(); ?></p></span>
								<input class="f-display font-normal" id="total_of_cart" hidden value="<?php echo WC()->cart->get_cart_contents_total() + WC()->cart->get_cart_contents_tax() ?>"></input>
							</div>
						</div>
					</section>

					<!-- <div class="flex justify-between">
						<p class="f-body">Discounts</p>
						<p class="f-body font-bold">X%</p>
					</div>
					<div class="flex justify-between">
						<p class="f-body">State Cannabis Excise Tax</p>
						<p class="f-body font-bold">$12</p>
					</div>
					<div class="flex justify-between">
						<p class="f-body">Delivery Fee</p>
						<p class="f-body font-bold">X%</p>
					</div>
					<div class="flex justify-between">
						<p class="f-body">Local Cannabis Business Fees</p>
						<p class="f-body font-bold">X%</p>
					</div>
					<div class="flex justify-between">
						<p class="f-body">Local Sales Tax</p>
						<p class="f-body font-bold">X%</p>
					</div> -->
				</div>
			</section>

			<?php do_action( 'woocommerce_checkout_order_review' ); ?>

		</section>

	</div>
</div>
</section>


</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout );?>