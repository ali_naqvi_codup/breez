<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class( '', $product ); ?>>

	<div class="related-products">
		<div class="content__wrapper">
			<article>
				<a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>" class="product-image" style="--aspect-ratio: (1 / 1)">

					<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );?>
					<img src="<?php  echo $image[0]; ?>" data-id="<?php echo $loop->post->ID; ?>">
				</a>
			</article>

			<article class="product-detail">
				<h4 class="mints-name"><?php the_title(); ?></h4>
				<a href="/category.html" class="product__type">
					<span x-text="mints.tag"></span>
				</a>
				<p class="mints-price">
					<?php $price = get_post_meta( get_the_ID(), '_regular_price', true); ?>
					<?php echo get_woocommerce_currency_symbol() . $price; ?>
				</p>
				<div>
					<span class="c-product-tag" x-text="mints.type">
						<?php echo wc_get_product_tag_list( $product->get_id(), ', ' ); ?>
					</span>
				</div>
			</article>

			<!-- Add to cart button  -->
			<a href="<?php echo get_site_url()."/cart?add-to-cart=".esc_attr($loop->post->ID); ?>" class="btn-primary add-to-cart">
				Add to Cart
			</a>
		</div>
	</li>
