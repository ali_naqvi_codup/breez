<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;
?>
<script>
	var ajaxurl = '<?= admin_url( 'admin-ajax.php' ) ?>';
</script>

<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>
	<div class="container lg:px-3-cols cart-heading">
		<h1>Cart</h1>
	</div>
	<?php
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
		$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
		$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
		$price=wc_get_price_including_tax( $_product );
		if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
			$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
			?>

			<section class="c-cart bg-white" x-data>
				<div class="container lg:px-3-cols">
					<div class="c-cart__inner">
						<article class="c-cart__item">
							<div class="w-1/2-cols">
								<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );?>
								<img src="<?php  echo $image[0]; ?>" data-id="<?php echo $loop->post->ID; ?>">
							</div>
							<div class="w-1/2-cols">
								<h2 class="f-body-lg font-bold mb-2">
									<?php
									if ( ! $product_permalink ) {
										echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
									} else {
										echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
									}

									do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

						// Meta data.
						echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.
						?>
					</h2>
					<p class="f-body">
						<?php
						$terms = get_the_terms( $product_id, 'product_cat' );
						$product_catz = '';
						foreach ($terms as $term) {
							$product_catz .= $term->name.', ';
						}
						echo rtrim($product_catz,',');
						?>

					</p>
					<p class="f-body-lg mt-2">
						<?php
								echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
								?>
							</p>
							<div class="c-quantity">
							<?php
							if ( $_product->is_sold_individually() ) {
								$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
							} else {
								$product_quantity = woocommerce_quantity_input(
									array(
										'input_name'   => "cart[{$cart_item_key}][qty]",
										'input_value'  => $cart_item['quantity'],
										'max_value'    => $_product->get_max_purchase_quantity(),
										'min_value'    => '0',
										'cart_item_key'=>$cart_item_key,
										'product_name' => $_product->get_name(),
										'product_id' => $product_id,
										'_product' => $_product,
										'price'=>$price,
									),
									$_product,
									false
								);
							}
							echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
						?> 
					</div>

				</div>
			</article>
		</div>

	</div>
</section> 
<?php
}
}
?>

<?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>

<?php do_action( 'woocommerce_before_cart_collaterals' ); ?>

<div class="cart-collaterals">
	<div class="container lg:px-3-cols">

		<?php do_action( 'woocommerce_cart_actions' ); ?>

		<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
		<?php
		/**
		 * Cart collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 */
		?>
		<div class="flex cart-total justify-between">
			<p class="total">Total</p>
			<p class="total">
				<span class="total_qty"><td data-title="<?php esc_attr_e( 'Subtotal', 'woocommerce' ); ?>"><?php wc_cart_totals_subtotal_html(); ?></td></span>
			</p>
			<input class="f-display font-normal" id="total_of_cart" hidden value="<?php echo WC()->cart->get_cart_contents_total() + WC()->cart->get_cart_contents_tax() ?>"></input>
		</div>

		<a href="<?php echo wc_get_checkout_url();?>" class="btn-primary block text-center mb-10"
			>To Checkout</a>
		</div>
	</div>

	<?php do_action( 'woocommerce_after_cart' ); ?>