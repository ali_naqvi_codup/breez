<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product;
global $post;
global $woocommerce;

get_header(); ?>

<!-- Product Images -->
<div class="cols-container">

	<div class="w-full lg:w-1/2-cols">
		<section class="c-single-product-images ratio-1x1 lg:sticky"
		>
		<!-- Slider main container -->
		<div class="owl-slider">
			<div id="carousel" class="owl-carousel">

				<?php
				$product_id = $post->ID;
				$product = new WC_product($product_id);
				$attachment_ids = $product->get_gallery_image_ids();

				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );?>
				<img src="<?php  echo $image[0]; ?>" data-id="<?php echo $loop->post->ID; ?>">

				<?php foreach( $attachment_ids as $attachment_id ) 
				{ ?>
					<div class="item product-image">
						
						<?php echo wp_get_attachment_image($attachment_id, 'full');?>
					</div>
				<?php }?>


			</div>
		</div>
	</section>

</div>

<div class="w-full lg:w-1/2-cols">
	<section class="c-product-main bg-white pt-10 lg:pt-40">
		<div class="container md:px-1-cols lg:px-0 xxl:w-7-cols">
			<article>
				<h3 class="f-display-sm font-normal text-primary">Hit your sweet spot</h3>
				<h1 class="f-display"><?php the_title( '<h1 class="product_title entry-title">', '</h1>' );
				?></h1>

				<div class="mt-4">
					<a href="" class="c-product-tag">Sativa</a>
				</div>

				<div class="flex divide-x-2 divide-shade-grey-200 mt-4 mb-4">
					<div class="space-y-1 pr-6">
						<p class="f-display font-normal">10mg</p>
						<p class="f-body">THC per mint</p>
					</div>
					<div class="space-y-2 pl-6">
						<p class="f-display font-normal">x30</p>
						<p class="f-body">mints</p>
					</div>
				</div>

				<div class="flex space-x-6 f-display">
					<?php $price = get_post_meta( get_the_ID(), '_regular_price', true);?>
					<p class="f-display">
						<?php echo get_woocommerce_currency_symbol() . $price; ?>
					</p>
				</div>
			</article>

			<form class="space-y-4 my-8" action="">
				<div class="c-input">
					<input name="zipcode" type="text" placeholder="Zip Code" />

					<div class="send-icon" style="top: 22px !important">
						<div>
							<img src="<?php echo get_template_directory_uri(); ?>/public/images/_send-icon.png">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri(); ?>/public/images/_send-icon.png">
						</div>
					</div>
				</div>

				<div class="cols-container">
					<div class=" w-1/2-cols width-half">
						<div class="c-quantity">
							<?php
							$product_quantity = woocommerce_quantity_input(
								array(
									'max_value'    => $product->get_max_purchase_quantity(),
									'min_value'    => '0',
									'product_name' => $product->get_name(),
								),
								$product,
								false
							);

							echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); 
							?> 
						</div>
					</div>

					<div class="w-1/2-cols">

						<?php	
						$args = array( 'post_type' => 'product' );
						$loop = new WP_Query( $args ); ?>

						<a href="<?php echo get_site_url()."/cart/?add-to-cart=".esc_attr($post->ID); ?>" class="btn-primary add-to-cart">
							Add to Cart
						</a>
					</div>
				</div>

				<button class="btn-shade-500 bg-shade" type="button">Find a Store</button>
			</form>
			<?php $short_description = apply_filters( 'woocommerce_short_description', $post->post_excerpt );

			if ( ! $short_description ) {
				return;
			}

			?>
			<div class="woocommerce-product-details__short-description">
				<?php echo $short_description; // WPCS: XSS ok. ?>
			</div>

		</div>
	</section>



	<section class="c-product-details bg-shade-grey-100 pt-4 lg:py-20">
		<div class="container md:px-1-cols lg:px-0 xxl:w-7-cols">
			<article>
				<?php			
				$heading = apply_filters( 'woocommerce_product_description_heading', __( 'Description', 'woocommerce' ) );
				?>
				<?php the_content(); ?>
			</article>
		</div>
	</section>

</div>
</div>

<section x-data class="store-finder">
	<div class="store-finder__inner">
		<div class="container lg:items-center">
			<div
			class="cols-container space-y-6 md:px-1-cols lg:px-0 lg:items-center"
			>
			<aside class="md:w-6/12-cols lg:w-5-cols store-finder__image">
				<?php
				if(is_active_sidebar('cannabis-image')){
					dynamic_sidebar('cannabis-image');
				}
				?>
			</aside>

			<div class="w-8-cols lg:w-5-cols lg:ml-1-cols space-y-10 xxl:w-4-cols">
				<article>
					<?php
					if(is_active_sidebar('cannabis-text')){
						dynamic_sidebar('cannabis-text');
					}
					?>
				</article>

				<div class="c-input">
					<?php echo do_shortcode('[gravityform id="1" title="false"]'); ?>
					<div class="send-icon">
						<div>
							<img src="<?php echo get_template_directory_uri(); ?>/public/images/_send-icon.png">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri(); ?>/public/images/_send-icon.png">
						</div>
					</div>
				</div>
				<a href="#">
					<button class="btn-secondary btn-shade-500" type="button">Order Delivery
					</button>
				</div>
			</div>
		</div>
	</div>
</section>


<section x-data="productSwiper" @resize.window.debounce.200ms="checkSlider"
class="bg-shade-grey-100 py-8 lg:pb-10 overflow-hidden related-products">
<div class="container">
	<h3 class="f-display w-1/2-cols mb-8">Customers also like</h3>
	<?php
	if( ! is_a( $product, 'WC_Product' ) ){
		$product = wc_get_product(get_the_id());
	}

	woocommerce_related_products( array(
		'posts_per_page' => 4,
		'columns'        => 4,
		'orderby'        => 'rand'
	) );?>
</div>
</section>

<?php
get_footer();