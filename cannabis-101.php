<?php

/**
 * Template Name: Cannabis-101
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();
?>

<!-- Secondry Nav -->
<div x-data class="c-secondary-nav">
  <div class="container">
    <nav>
      <ul>
        <li>
          <a
          :class="activeNav === 'precise-dosing' ? 'is-active': ''"
          href="#precise-dosing"
          >Precise Dosing</a
          >
        </li>
        <li>
          <a
          :class="activeNav === 'cannabinoids' ? 'is-active': ''"
          href="#cannabinoids"
          >Cannabinoids</a
          >
        </li>
        <li>
          <a
          :class="activeNav === 'full-spectrum' ? 'is-active': ''"
          href="#full-spectrum"
          >Full-spectrum</a
          >
        </li>
        <li>
          <a
          :class="activeNav === 'sublingual' ? 'is-active': ''"
          href="#sublingual"
          >Sublingual delivery</a
          >
        </li>
        <li>
          <a
          :class="activeNav === 'terpenes' ? 'is-active': ''"
          href="#terpenes"
          >Terpenes</a
          >
        </li>
        <li>
          <a
          :class="activeNav === 'e-system' ? 'is-active': ''"
          href="#e-system"
          >Endocannabinoid System</a
          >
        </li>
      </ul>
    </nav>
  </div>
</div>

<!-- Hero section -->
<section class="hero-101">
  <div class="container md:px-1-cols lg:px-0">
    <div class="hero-101__inner py-20 lg:py-40">
      <div class="cols-container">
        <article
        class="hero-101__icon mb-10 w-6-cols md:w-8-cols lg:w-6-cols lg:mb-0"
        >
        <img src="<?php echo get_field('cannabis_101_icon') ?>" alt="" />
      </article>

      <article
      class="hero-101__content w-6-cols md:w-8-cols lg:w-5-cols xxl:w-4-cols"
      >
      <h1><?php echo get_field('cannabis_101_heading') ?></h1>
      <p>
        <?php echo get_field('cannabis_101_text') ?>
      </p>

      <a href="#precise-dosing">
        <button class="btn-primary" type="button"><?php echo get_field('cannabis_101_button_text') ?></button>
      </a>
    </article>
  </div>
</div>
</div>
</section>

<!-- Precise Dosing -->

<section
id="precise-dosing"
class="c-alt-columns bg-shade"
:class="activeNav === $el.id ? 'is-active' : ''"
x-intersect:enter.half="activeNav = $el.id"
>
<div class="container md:px-1-cols lg:px-0">
  <div class="c-alt-columns__inner">
    <div class="cols-container">
      <article class="column-content w-6-cols md:w-8-cols lg:w-5-cols">
        <h2><?php echo get_field('precise_dosing_heading') ?></h2>
        <p>
         <?php echo get_field('precise_dosing_text_1') ?>
       </p>
       <p>
         <?php echo get_field('precise_dosing_text_2') ?>
       </p>
     </article>
     <article
     class="column-icon w-6-cols md:w-8-cols lg:w-5-cols lg:ml-2-cols"
     >
     <img src="<?php echo get_field('precise_dosing_icon') ?>" alt="" />
   </article>
 </div>
</div>
</div>
</section>

<!-- Cannabinoids Start -->

<section
id="cannabinoids"
class="c-alt-columns"
:class="activeNav === $el.id ? 'is-active' : ''"
x-intersect:enter.half="activeNav = $el.id"
>
<div class="container md:px-1-cols lg:px-0">
  <div class="c-alt-columns__inner">
    <div class="cols-container alternate">
      <article
      class="column-content w-6-cols md:w-8-cols lg:w-5-cols lg:mr-1-cols"
      >
      <div class="mb-10">
        <h2> <?php echo get_field('cannabinoids_heading') ?></h2>
        <p>
         <?php echo get_field('cannabinoids_text') ?>
       </p>
     </div>

     <!-- Tab section -->

     <section class="c-tabs mb-10">
     <ul class="cannabistabs">
        <li class="active">
          <a href="javascript:void(0);"><?php echo get_field('cannabinoids_tab_1_heading') ?></a>
        </li>
        <li>
          <a href="javascript:void(0);"><?php echo get_field('cannabinoids_tab_2_heading') ?></a>
        </li>
        <li>
          <a href="javascript:void(0);"><?php echo get_field('cannabinoids_tab_3_heading') ?></a>
        </li>
      </ul>
		 
      <ul class="tab__content">
        <li class="active">
          <div class="content__wrapper">
            <article x-show="openTab === 1" class="open-tab">
              <div class="tab-row top">
                <div class="tab-icon">
                  <img src="<?php echo get_field('cannabinoids_tab_tick_icon') ?>"></include>
                </div>
                <div class="tab-copy">
                  <p><li><?php echo get_field('cannabinoids_tab_1_text') ?></li></p>
                </div>
              </div>

              <div class="tab-row sub-title">
                <div class="tab-title">
                  <h4><?php echo get_field('cannabinoids_tab_1_list_heading') ?></h4>
                </div>
              </div>

              <div class="tab-row">
                <article class="flex items-center flex-1">
                  <div class="tab-icon">
                   <img src="<?php echo get_field('cannabinoids_tab_tick_icon') ?>"></include>
                 </div>
                 <div class="tab-copy">
                  <p><?php echo get_field('cannabinoids_tab_1_list_1') ?></p>
                </div>
              </article>

              <article class="flex items-center flex-1">
                <div class="tab-icon">
                 <img src="<?php echo get_field('cannabinoids_tab_tick_icon') ?>"></include>
               </div>
               <div class="tab-copy">
                <p><?php echo get_field('cannabinoids_tab_1_list_2') ?></p>
              </div>
            </article>
          </div>

          <div class="tab-row">
            <article class="flex items-center flex-1">
              <div class="tab-icon">
               <img src="<?php echo get_field('cannabinoids_tab_tick_icon') ?>"></include>
             </div>
             <div class="tab-copy">
              <p><?php echo get_field('cannabinoids_tab_1_list_3') ?></p>
            </div>
          </article>

          <article class="flex items-center flex-1">
            <div class="tab-icon">
              <img src="<?php echo get_field('cannabinoids_tab_tick_icon') ?>"></include>
            </div>
            <div class="tab-copy">
              <p><?php echo get_field('cannabinoids_tab_1_list_4') ?></p>
            </div>
          </article>
        </div>
      </article>
    </div>
  </li>
  <li>
    <div class="content__wrapper">
      <article x-show="openTab === 2" class="open-tab">
        <div class="tab-row top">
          <div class="tab-icon">
           <img src="<?php echo get_field('cannabinoids_tab_cross_icon') ?>"></include>
         </div>
         <div class="tab-copy">
          <p><?php echo get_field('cannabinoids_tab_2_text') ?></p>
        </div>
      </div>

      <div class="tab-row sub-title">
        <div class="tab-title">
          <h4><?php echo get_field('cannabinoids_tab_2_list_heading') ?></h4>
        </div>
      </div>

      <div class="tab-row">
        <article class="flex items-center flex-1">
          <div class="tab-icon">
           <img src="<?php echo get_field('cannabinoids_tab_tick_icon') ?>"></include>
         </div>
         <div class="tab-copy">
          <p><?php echo get_field('cannabinoids_tab_2_list_1') ?></p>
        </div>
      </article>

      <article class="flex items-center flex-1">
        <div class="tab-icon">
          <img src="<?php echo get_field('cannabinoids_tab_tick_icon') ?>"></include>
        </div>
        <div class="tab-copy">
          <p><?php echo get_field('cannabinoids_tab_2_list_2') ?></p>
        </div>
      </article>
    </div>

    <div class="tab-row">
      <article class="flex items-center flex-1">
        <div class="tab-icon">
         <img src="<?php echo get_field('cannabinoids_tab_tick_icon') ?>"></include>
       </div>
       <div class="tab-copy">
        <p><?php echo get_field('cannabinoids_tab_2_list_3') ?></p>
      </div>
    </article>

    <article class="flex items-center flex-1">
      <div class="tab-icon">
        <img src="<?php echo get_field('cannabinoids_tab_tick_icon') ?>"></include>
      </div>
      <div class="tab-copy">
        <p><?php echo get_field('cannabinoids_tab_2_list_4') ?></p>
      </div>
    </article>
  </div>
</article>
</div>
</li>
<li>
  <div class="content__wrapper">
    <article x-show="openTab === 3" class="open-tab">
      <div class="tab-row top">
        <div class="tab-icon">
         <img src="<?php echo get_field('cannabinoids_tab_cross_icon') ?>"></include>
       </div>
       <div class="tab-copy">
        <p><?php echo get_field('cannabinoids_tab_3_text') ?></p>
      </div>
    </div>

    <div class="tab-row sub-title">
      <div class="tab-title">
        <h4><?php echo get_field('cannabinoids_tab_3_list_heading') ?></h4>
      </div>
    </div>

    <div class="tab-row">
      <article class="flex items-center flex-1">
        <div class="tab-icon">
         <img src="<?php echo get_field('cannabinoids_tab_tick_icon') ?>"></include>
       </div>
       <div class="tab-copy">
        <p><?php echo get_field('cannabinoids_tab_3_list_1') ?></p>
      </div>
    </article>
  </div>
</article>
</div>
</li>
</ul>
</section>

<div class="accordion_container">
  <div class="accordion_head">References<span class="plusminus">+</span></div>
  <div class="accordion_body" style="display: none;">
   <div class="block-text__inner">
    <p>
      Burstein, Sumner.
      <a
      class="text-primary"
      href="https://www.sciencedirect.com/science/article/abs/pii/S0968089615000838?via%3Dihub"
      >“Cannabidiol (CBD) and its analogs: a review of their
      effects on inflammation.”
    </a>
    Bioorganic & Medicinal Chemistry vol. 23,7 (2015):
    1377-1385.
  </p>

  <p>
    El-Alfy, Abir T et al.
    <a
    class="text-primary"
    href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2866040/"
    >
    “Antidepressant-like effect of delta9-tetrahydrocannbinol
    and other cannabinoids isolated from Cannabis sativa L.”
  </a>
  Pharmacology, biochemistry, and behavior vol. 95,4 (2010):
  434-42. doi:10.1016/j.ppb.2010.03.004
</p>

<p>
  Johnson, Jeremy et al.
  <a
  class="text-primary"
  href="https://www.jpsmjournal.com/article/S0885-3924(09)00787-8/fulltext"
  >“Multicenter, Double-blind, Randomized,
  Placebo-Controlled, Parallel-Group Study of the Efficacy,
  Safety and Tolerability of THC:CBD Extract and THC Extract
  in Patients with Intractable Cancer-Related Pain.”
</a>
Journal of Pain and Symptom Management. vol. 39/2 (2010):
167-179
</p>

<p>
  Russo, Ethan B.
  <a
  class="text-primary"
  href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3165946/"
  >
  “Taming THC: potential cannabis synergy and
  phytocannabinoid-terpenoid entourage effects.”
</a>
British journal of pharmacology vol. 163,7 (2011): 1344-64.
doi:10.1111/j.1476-5381.2011.01238.x
</p>

<p>
  Saito, Viviane M et al.
  <a
  class="text-primary"
  href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3386505/"
  >
  “Cannabinoid modulation of neuroinflammatory disorders.”
</a>
Current neuropharmacology vol. 10,2 (2012): 159-66.
doi:10.2174/157015912800604515
</p>

<p>
  Tambaro, Simone, and Marco Bortolato.
  <a
  class="text-primary"
  href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3691841/"
  >
  “Cannabinoid-related agents in the treatment of anxiety
  disorders: current knowledge and future perspectives.”
</a>
Recent patents on CNS drug discovery vol. 7,1 (2012): 25-40.
</p>
</div>
</div>
</div>


<!-- Refs section -->
<!-- <div x-data="accordion" class="c-faq__block global">
  <article class="c-faq__block-accordion">
    <button
    @click="toggle(0)"
    class="acc-btn"
    :class="open === 0 ? 'active' : ''"
    >
    <h4>References</h4>

    <div class="accordion-icon">
      <span x-text="open === 0 ? '-' : '+'"></span>
    </div>
  </button>

  <div class="c-faq__block-text" x-show="open === 0" x-collapse>

</div>
</article>
</div> -->
</article>

 <article class="w-6-cols md:w-8-cols lg:w-5-cols">
  <img src="<?php echo get_field('cannabinoids_icon') ?>" alt="" /> 
</article>
</div>
</div>
</div>
</section>

<!-- Full Spectrum Start -->

<section
id="full-spectrum"
class="c-alt-columns bg-shade"
:class="activeNav === $el.id ? 'is-active' : ''"
x-intersect:enter.half="activeNav = $el.id"
>
<div class="container md:px-1-cols lg:px-0">
  <div class="c-alt-columns__inner">
    <div class="cols-container">
      <article class="column-content w-6-cols md:w-8-cols lg:w-5-cols">
        <h2><?php echo get_field('full_spectrum_heading') ?></h2>
        <p>
          <?php echo get_field('full_spectrum_text_1') ?>
        </p>
        <p>
          <?php echo get_field('full_spectrum_text_2') ?>
        </p>
      </article>
      <article
      class="column-icon w-6-cols md:w-8-cols lg:w-5-cols lg:ml-2-cols"
      >
      <img src="<?php echo get_field('full_spectrum_icon') ?>" alt="" /> 
    </article>
  </div>
</div>
</div>
</section>

<!-- Sublingual Start -->
<section
id="sublingual"
class="c-alt-columns"
:class="activeNav === $el.id ? 'is-active' : ''"
x-intersect:enter.half="activeNav = $el.id"
>
<div class="container md:px-1-cols lg:px-0">
  <div class="c-alt-columns__inner">
    <div class="cols-container alternate">
      <article
      class="column-content w-6-cols md:w-8-cols lg:w-5-cols lg:mr-1-cols"
      >
      <h2><?php echo get_field('sublingual_heading') ?></h2>
      <p>
        <?php echo get_field('sublingual_text_1') ?>
      </p>
      <p>
       <?php echo get_field('sublingual_text_2') ?>
     </p>
   </article>
   <article class="column-icon w-6-cols md:w-8-cols lg:w-5-cols">
    <img src="<?php echo get_field('sublingual_icon') ?>" alt="" /> 
  </article>
</div>
</div>
</div>
</section>

<!-- Terpenes Start -->
<section
id="terpenes"
class="c-alt-columns bg-shade"
:class="activeNav === $el.id ? 'is-active' : ''"
x-intersect:enter.half="activeNav = $el.id"
>
<div class="container md:px-1-cols lg:px-0">
  <div class="c-alt-columns__inner">
    <div class="cols-container">
      <article class="column-content w-6-cols md:w-8-cols lg:w-5-cols">
        <h2><?php echo get_field('terpenes_heading') ?></h2>
        <p>
          <?php echo get_field('terpenes_text_1') ?>
        </p>
        <p>
         <?php echo get_field('terpenes_text_2') ?>
       </p>
     </article>
     <article
     class="column-icon w-6-cols md:w-8-cols lg:w-5-cols lg:ml-2-cols"
     >
     <img src="<?php echo get_field('terpenes_icon') ?>" alt="" /> 
   </article>
 </div>
</div>
</div>
</section>

<!-- Endocannabinoid System Start -->
<section
id="e-system"
class="c-alt-columns"
:class="activeNav === $el.id ? 'is-active' : ''"
x-intersect:enter.half="activeNav = $el.id"
>
<div class="container md:px-1-cols lg:px-0">
  <div class="c-alt-columns__inner">
    <div class="cols-container alternate">
      <article
      class="column-content w-6-cols md:w-8-cols lg:w-5-cols lg:mr-1-cols"
      >
      <h2><?php echo get_field('e_system_heading') ?></h2>
      <p>
       <?php echo get_field('e_system_text_1') ?>
     </p>
     <p>
      <?php echo get_field('e_system_text_2') ?>
    </p>
  </article>
  <article class="column-icon w-6-cols md:w-8-cols lg:w-5-cols">
   <img src="<?php echo get_field('e_system_icon') ?>" alt="" /> 
 </article>
</div>
</div>
</div>
</section>



<!-- Testimonial Section -->
<section x-data="testimonialSwiper" class="c-testimonial overflow-hidden py-20 lg:py-40">
    <div class="container md:px-1-cols lg:px-0">
        <div class="c-testimonial__header">
            <h2 class="c-testimonial__title">Hear it From Our Fans.</h2>

            <article class="c-testimonial__icon">
                <img src="<?php echo get_template_directory_uri(); ?>/public/images/testimonial_icon.png" alt="" />
            </article>
        </div>
      </div>

        <!-- Slider main container -->
        <article class="c-testimonial-swiper">
            <!-- Additional required wrapper -->
            <div id="testimonial-carousel" class="tincture-sprays-carousel box-shadow owl-carousel">
                <!-- Slides -->

                <?php
                $args = array( 'post_type' => 'testimonials', 'posts_per_page' => 4 );
                $loop = new WP_Query( $args );
                while ( $loop->have_posts() ) : $loop->the_post(); ?>

                    <div class="swiper-slide project-item">
                        <div class="swiper__inner post-slide">
                            <?php echo get_the_content(); ?>                       

                    </div>
                </div>

            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
        </div>
    </article>
</section>

<?php
get_footer();
?>