$(document).ready(function() {
    "use strict";

    /*=========================================================================
    Mints Carousel
    =========================================================================*/
    $('#projects-carousel').owlCarousel({
        loop: false,
        margin: 0,
        autoplay: false,
        smartSpeed: 500,
        nav: true,
        navText: ['<i class="fa fa-angle-left">', '<i class="fa fa-angle-right">'],
        dots: false,
        responsive : {
            0 : {
                items: 1
            },
            580 : {
                items: 2,
            },
            768 : {
                items: 2,
            },
            992 : {
                items: 4,
            }
        }
    });


    $("#carousel").owlCarousel({
      autoplay: true,
      rewind: true, /* use rewind if you don't want loop */
      margin: 20,
      responsiveClass: true,
      autoHeight: true,
      autoplayTimeout: 7000,
      smartSpeed: 800,
      dots: true,
      responsive: {
        0: {
          items: 1
      },

      600: {
          items: 3
      },

      1024: {
          items: 1
      },

      1366: {
          items: 1
      }
  }
});

    /*=========================================================================
    Tincture Carousel
    =========================================================================*/
    $('#tincture-sprays-carousel').owlCarousel({
        loop: false,
        margin: 0,
        autoplay: false,
        smartSpeed: 500,
        nav: true,
        navText: ['<i class="fa fa-angle-left">', '<i class="fa fa-angle-right">'],
        dots: false,
        responsive : {
            0 : {
                items: 1
            },
            580 : {
                items: 2,
            },
            768 : {
                items: 2,
            },
            992 : {
                items: 4,
            }
        }
    });

 /*=========================================================================
    Tablets Carousel
    =========================================================================*/
    $('#tablets-carousel').owlCarousel({
        loop: false,
        margin: 0,
        autoplay: false,
        smartSpeed: 500,
        nav: true,
        navText: ['<i class="fa fa-angle-left">', '<i class="fa fa-angle-right">'],
        dots: false,
        responsive : {
            0 : {
                items: 1
            },
            580 : {
                items: 2,
            },
            768 : {
                items: 2,
            },
            992 : {
                items: 4,
            }
        }
    });

       /*=========================================================================
    Testimonial Carousel
    =========================================================================*/
    $('#testimonial-carousel').owlCarousel({
       loop:false,
       navRewind:false,
        margin: 0,
        autoplay: false,
        smartSpeed: 500,
        nav: true,
        navText: ['<i class="fa fa-angle-left">', '<i class="fa fa-angle-right">'],
        dots: false,
        responsive : {
            0 : {
                items: 1
            },
            580 : {
                items: 2,
            },
            768 : {
                items: 2,
            },
            992 : {
                items: 3,
            }
        }
    });




});



$(function() {
    var Accordion = function(el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        // Variables privadas
        var links = this.el.find('.link');
        // Evento
        links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
    }

    Accordion.prototype.dropdown = function(e) {
        var $el = e.data.el;
        $this = $(this),
        $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
        };
    }   

    var accordion = new Accordion($('#accordion'), false);
});


$(document).ready(function() {

    $(".search-icon").click(function() {
       $(".togglesearch").toggle();
       $(".togglesearch input[type='text']").focus();
   });

});


$(document).ready(function(){
    
    // Variables
    var clickedTab = $(".tabs > .active");
    var tabWrapper = $(".tab__content");
    var activeTab = tabWrapper.find(".active");
    var activeTabHeight = activeTab.outerHeight();
    
    // Show tab on page load
    activeTab.show();
    
    // Set height of wrapper on page load
    tabWrapper.height(activeTabHeight);
    
    $(".tabs > li").on("click", function() {
        
        // Remove class from active tab
        $(".tabs > li").removeClass("active");
        
        // Add class active to clicked tab
        $(this).addClass("active");
        
        // Update clickedTab variable
        clickedTab = $(".tabs .active");
        
        // fade out active tab
        activeTab.fadeOut(250, function() {
            
            // Remove active class all tabs
            $(".tab__content > li").removeClass("active");
            
            // Get index of clicked tab
            var clickedTabIndex = clickedTab.index();

            // Add class active to corresponding tab
            $(".tab__content > li").eq(clickedTabIndex).addClass("active");
            
            // update new active tab
            activeTab = $(".tab__content > .active");
            
            // Update variable
            activeTabHeight = activeTab.outerHeight();
            
            // Animate height of wrapper to new tab height
            tabWrapper.stop().delay(50).animate({
                height: activeTabHeight
            }, 500, function() {
                
                // Fade in active tab
                activeTab.delay(50).fadeIn(250);
                
            });
        });
    });
});

$(document).ready(function(){
    
    // Variables
    var clickedTab = $(".cannabistabs > .active");
    var tabWrapper = $(".tab__content");
    var activeTab = tabWrapper.find(".active");
    var activeTabHeight = activeTab.outerHeight();
    
    // Show tab on page load
    activeTab.show();
    
    // Set height of wrapper on page load
    tabWrapper.height(activeTabHeight);
    
    $(".cannabistabs > li").on("click", function() {
        
        // Remove class from active tab
        $(".cannabistabs > li").removeClass("active");
        
        // Add class active to clicked tab
        $(this).addClass("active");
        
        // Update clickedTab variable
        clickedTab = $(".cannabistabs .active");
        
        // fade out active tab
        activeTab.fadeOut(250, function() {
            
            // Remove active class all tabs
            $(".tab__content > li").removeClass("active");
            
            // Get index of clicked tab
            var clickedTabIndex = clickedTab.index();

            // Add class active to corresponding tab
            $(".tab__content > li").eq(clickedTabIndex).addClass("active");
            
            // update new active tab
            activeTab = $(".tab__content > .active");
            
            // Update variable
            activeTabHeight = activeTab.outerHeight();
            
            // Animate height of wrapper to new tab height
            tabWrapper.stop().delay(50).animate({
                height: activeTabHeight
            }, 500, function() {
                
                // Fade in active tab
                activeTab.delay(50).fadeIn(250);
                
            });
        });
    });
});


$(document).ready(function() {
  //toggle the component with class accordion_body
  $(".accordion_head").click(function() {
    if ($('.accordion_body').is(':visible')) {
      $(".accordion_body").slideUp(300);
      $(".plusminus").text('+');
  }
  if ($(this).next(".accordion_body").is(':visible')) {
      $(this).next(".accordion_body").slideUp(300);
      $(this).children(".plusminus").text('+');
  } else {
      $(this).next(".accordion_body").slideDown(300);
      $(this).children(".plusminus").text('-');
  }
});
});

  // Uses the Google maps api callback to initialise the Alpine component when ready
  window.initMap = function() {
    var interval = setInterval(function() {
        window.dispatchEvent(new CustomEvent("init-map"));
        clearInterval(interval);
    }, 200);
}

    $(document).ready(function() {
        $("input[id^='quantity_']").each(function (){
            var plus = $(this).val();
            if (plus>=2) {
                $(this).siblings('.minus').show();
                $(this).siblings('.bin-icon').hide();
            }
            else{
                $(this).siblings('.minus').hide();
                $(this).siblings('.bin-icon').show();
            }
        });
        var plus = $('.plus').val();
        
    });

    function plus(elem){
        var id=$(elem).attr('data-id');
        // var total=$("#total_of_cart").val();
        var item_id=$(elem).attr('data-item_id');
        var number_id=$('#'+id).val();
        //total=parseInt(total)+parseInt(price);
        if($('#'+id).val()<=1)
        {
            $('#minus_'+id).show();
            $('#bin_'+id).hide();
        }else{
            $('#bin_'+id).hide();
            $('#minus_'+id).show();
        }
        $('#'+id).val(parseInt(number_id)+1);
        $.ajax({
            url: ajaxurl,
            dataType : "json",
            data: {
                'action':'set_content',
                'item_id': parseInt(item_id),
                'quantity':parseInt(number_id)+1
            },
            type: 'post',
            success: function(result) {
               if(result==1)
               window.location.href=window.location.href.split('?')[0];
            }
        });
    }


    function minus(elem){
        // var total=$("#total_of_cart").val();
        var id=$(elem).attr('data-id');
        var _val = parseInt($('#'+id).val());
        var item_id=$(elem).attr('data-item_id');
        var number_id=$('#'+id).val();
        //total=parseInt(total)+parseInt(price);
        $.ajax({
            url: ajaxurl,
            dataType : "json",
            data: {
                'action':'set_content',
                'item_id': parseInt(item_id),
                'quantity':parseInt(number_id)-1
            },
            type: 'post',
            success: function(result) {
               if(result==1)
               location.reload();
            }
        });
        if(_val<3)
        {
            $('#bin_'+id).show();
            $('#minus_'+id).hide();
        }else{
            $('#bin_'+id).hide();
            $('#minus_'+id).show();
        }
        $('#'+id).val(number_id-1);
    }