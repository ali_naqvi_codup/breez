$( document ).ready(function() {
    //menu item hover image change
    $('.c-menu__nav  a').hover(function(){
      var imgpath =  $(this).attr("data-image");
      $('.menu-image').attr('src', imgpath);
    })


  });

function openMenu(){ 
  $('.c-menu').show();
  $('.c-menu').removeAttr('x-cloak');
  $('body').css('overflow', 'hidden');
}
function closeMenu(){ 
  if (!$('.c-menu').attr('x-cloak')) {
    $('.c-menu').hide();
    $('body').css('overflow', 'scroll');
  }
}

