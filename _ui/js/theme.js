import {
    d as c,
    e as g,
    g as n,
    S as p,
    a as l,
    N as d,
    P as m,
    m as r,
    b as u,
    c as M
} from "./vendor.99ff0246.js";
const v = function() {
    const t = document.createElement("link").relList;
    if (t && t.supports && t.supports("modulepreload")) return;
    for (const a of document.querySelectorAll('link[rel="modulepreload"]')) s(a);
    new MutationObserver(a => {
        for (const o of a)
            if (o.type === "childList")
                for (const y of o.addedNodes) y.tagName === "LINK" && y.rel === "modulepreload" && s(y)
    }).observe(document, {
        childList: !0,
        subtree: !0
    });

    function i(a) {
        const o = {};
        return a.integrity && (o.integrity = a.integrity), a.referrerpolicy && (o.referrerPolicy = a.referrerpolicy), a.crossorigin === "use-credentials" ? o.credentials = "include" : a.crossorigin === "anonymous" ? o.credentials = "omit" : o.credentials = "same-origin", o
    }

    function s(a) {
        if (a.ep) return;
        a.ep = !0;
        const o = i(a);
        fetch(a.href, o)
    }
};
v();
var h = () => ({
        name: "ageGate",
        open: !1,
        close() {
            sessionStorage.ageGate = "true", sessionStorage.ageGate && (this.open = !1)
        },
        init() {
            const e = {
                reserveScrollBarGap: !0
            };
            this.$watch("open", t => {
                t === !0 ? c(this.$el, e) : g(this.$el)
            }), sessionStorage.ageGate || (this.open = !0)
        }
    }),
    S = () => ({
        name: "productSwiper",
        swiper: null,
        currentSlider: 0,
        essentials: [{
            image: "/Sprays/Breez-Sprays-BerryDaytime.png",
            name: "Berry Nighttime",
            price: "$30",
            tag: "Spray",
            type: "Sativa"
        }, {
            image: "/Mints/Breez-Mints-RoyalMint-Hybrid.png",
            name: "Royal Mint",
            price: "$30",
            tag: "Mint",
            type: "Active"
        }, {
            image: "/Mints/Breez-Mints-Cinnamon1-1.png",
            name: "Cinnamon Relief",
            price: "$30",
            tag: "Mint",
            type: "Sativa"
        }, {
            image: "/Sprays/Breez-Sprays-CitrusRecovery.png",
            name: "Citrus Recovery",
            price: "$30",
            tag: "Spray",
            type: "Active"
        }, {
            image: "/Sprays/Breez-Sprays-BerryDaytime.png",
            name: "Berry Daytime",
            price: "$30",
            tag: "Spray",
            type: "Sativa"
        }, {
            image: "/Mints/Breez-Mints-OriginalMint.png",
            name: "Original Mint",
            price: "$30",
            tag: "Mint",
            type: "Active"
        }],
        newly: [{
            image: "/Mints/Breez-Mints-RoyalMint-Hybrid.png",
            name: "Royal Mint",
            price: "$30",
            tag: "Mint",
            type: "Active"
        }, {
            image: "/Mints/Breez-Mints-OriginalMint.png",
            name: "Original Mint",
            price: "$30",
            tag: "Mint",
            type: "Active"
        }, {
            image: "/Sprays/Breez-Sprays-CitrusRecovery.png",
            name: "Citrus Recovery",
            price: "$30",
            tag: "Spray",
            type: "Active"
        }, {
            image: "/Mints/Breez-Mints-RoyalMint-Hybrid.png",
            name: "Royal Mint",
            price: "$30",
            tag: "Mint",
            type: "Active"
        }],
        popular: [{
            image: "/Mints/Breez-Mints-RoyalMint-Hybrid.png",
            name: "Royal Mint",
            price: "$30",
            tag: "Mint",
            type: "Active"
        }, {
            image: "/Sprays/Breez-Sprays-BerryDaytime.png",
            name: "Berry Nighttime",
            price: "$30",
            tag: "Spray",
            type: "Sativa"
        }, {
            image: "/Mints/Breez-Mints-OriginalMint.png",
            name: "Original Mint",
            price: "$30",
            tag: "Mint",
            type: "Active"
        }, {
            image: "/Sprays/Breez-Sprays-CitrusRecovery.png",
            name: "Citrus Recovery",
            price: "$30",
            tag: "Spray",
            type: "Active"
        }],
        toggle(e) {
            this.currentSlider = e, this.checkSlider()
        },
        gsapSet(e) {
            this.$nextTick(() => {
                e.querySelectorAll(".lg-feature").forEach(i => {
                    n.set(i, {
                        opacity: 0,
                        y: 10
                    })
                })
            })
        },
        gsapEntry(e) {
            this.$nextTick(() => {
                const t = e.querySelectorAll(".lg-feature");
                n.to(t, {
                    opacity: 1,
                    y: 0,
                    stagger: .175,
                    ease: "power2.out"
                })
            })
        },
        checkSlider() {
            this.swiper && this.swiper.destroy(), this.$nextTick(() => {
                window.matchMedia("(max-width: 1023px)").matches ? this.swiper = new p(this.$root.querySelector(".c-swiper"), {
                    grabCursor: !0,
                    preloadImages: !0,
                    preventClicks: !0,
                    preventClicksPropagation: !0,
                    slidesPerView: "auto",
                    noSwiping: !0,
                    noSwipingClass: "swiper-no-swiping"
                }) : this.swiper && this.swiper.destroy()
            })
        },
        init() {
            this.$nextTick(() => {
                this.checkSlider()
            })
        }
    });
n.registerPlugin(l);
var f = () => ({
    name: "flavoursGrid",
    products: [{
        name: "Berry Daytime",
        image: "/Sprays/Breez-Sprays-BerryDaytime.png",
        hoverColor: "#FA6A85"
    }, {
        name: "Royal Mint",
        image: "/Mints/Breez-Mints-RoyalMint-Hybrid.png",
        hoverColor: "#ABD9DD"
    }, {
        name: "Cinnamon Relief",
        image: "/Mints/Breez-Mints-Cinnamon1-1.png",
        hoverColor: "#F43344"
    }, {
        name: "Citrus Recovery",
        image: "/Sprays/Breez-Sprays-CitrusRecovery.png",
        hoverColor: "#FEB561"
    }, {
        name: "Berry Nighttime",
        image: "/Sprays/Breez-Sprays-BerryNighttime.png",
        hoverColor: "#AB5178"
    }, {
        name: "Hybrid",
        image: "/Tablets/Breez-Tablets-Hybrid.png",
        hoverColor: "#ABD9DD"
    }],
    viewMoreFlavours() {
        document.querySelectorAll(".flavours__card").forEach((i, s) => {
            s < 4 && i.classList.remove("is-hidden")
        }), document.querySelectorAll(".flavours__card").length === 0 && this.$el.classList.add("is-hidden")
    },
    init() {
        this.$nextTick(() => {
            document.querySelectorAll(".flavours__card").forEach((t, i) => {
                i > 5 && t.classList.add("is-hidden")
            })
        })
    }
});
n.registerPlugin(l);
var w = () => ({
    name: "categoryGrid",
    products: [{
        image: "/Mints/Breez-Mints-BerryDaytime.png",
        name: "Berry Daytime",
        price: "$30",
        tag: "Mint",
        type: "Active"
    }, {
        image: "/Mints/Breez-Mints-BerryNighttime.png",
        name: "Berry Nighttime",
        price: "$30",
        tag: "Mint",
        type: "Sativa"
    }, {
        image: "/Mints/Breez-Mints-Cinnamon1-1.png",
        name: "Cinnamon 1:1",
        price: "$30",
        tag: "Mint",
        type: "Active"
    }, {
        image: "/Mints/Breez-Mints-CitrusRecovery.png",
        name: "Citrus Recovery",
        price: "$30",
        tag: "Mint",
        type: "Active"
    }, {
        image: "/Mints/Breez-Mints-OriginalMint.png",
        name: "Original Mint",
        price: "$30",
        tag: "Mint",
        type: "Active"
    }, {
        image: "/Mints/Breez-Mints-RoyalMint-Hybrid.png",
        name: "Royal Mint",
        price: "$30",
        tag: "Mint",
        type: "Hybrid"
    }, {
        image: "/Mints/Breez-Mints-RoyalMint-Indica.png",
        name: "Royal Mint",
        price: "$30",
        tag: "Mint",
        type: "Indica"
    }, {
        image: "/Mints/Breez-Mints-RoyalMint-Sativa.png",
        name: "Royal Mint",
        price: "$30",
        tag: "Mint",
        type: "Sativa"
    }],
    viewMoreFlavours() {
        document.querySelectorAll(".flavours__card").forEach((i, s) => {
            s < 4 && i.classList.remove("is-hidden")
        }), document.querySelectorAll(".flavours__card").length === 0 && this.$el.classList.add("is-hidden")
    },
    init() {
        this.$nextTick(() => {
            document.querySelectorAll(".flavours__card").forEach((t, i) => {
                i > 5 && t.classList.add("is-hidden")
            })
        })
    }
});
n.registerPlugin(l);
var B = () => ({
    name: "productCollectionSwiper",
    swiper: null,
    mints: [{
        image: "/Mints/Breez-Mints-BerryDaytime.png",
        name: "Berry Daytime",
        price: "$30",
        tag: "Mint",
        type: "Active"
    }, {
        image: "/Mints/Breez-Mints-BerryNighttime.png",
        name: "Berry Nighttime",
        price: "$30",
        tag: "Mint",
        type: "Sativa"
    }, {
        image: "/Mints/Breez-Mints-Cinnamon1-1.png",
        name: "Cinnamon 1:1",
        price: "$30",
        tag: "Mint",
        type: "Active"
    }, {
        image: "/Mints/Breez-Mints-CitrusRecovery.png",
        name: "Citrus Recovery",
        price: "$30",
        tag: "Mint",
        type: "Active"
    }, {
        image: "/Mints/Breez-Mints-OriginalMint.png",
        name: "Original Mint",
        price: "$30",
        tag: "Mint",
        type: "Active"
    }, {
        image: "/Mints/Breez-Mints-RoyalMint-Hybrid.png",
        name: "Royal Mint",
        price: "$30",
        tag: "Mint",
        type: "Hybrid"
    }, {
        image: "/Mints/Breez-Mints-RoyalMint-Indica.png",
        name: "Royal Mint",
        price: "$30",
        tag: "Mint",
        type: "Indica"
    }, {
        image: "/Mints/Breez-Mints-RoyalMint-Sativa.png",
        name: "Royal Mint",
        price: "$30",
        tag: "Mint",
        type: "Sativa"
    }],
    sprays: [{
        image: "/Sprays/Breez-Sprays-BerryDaytime.png",
        name: "Berry Daytime",
        price: "$30",
        tag: "Spray",
        type: "Active"
    }, {
        image: "/Sprays/Breez-Sprays-BerryNighttime.png",
        name: "Berry Nighttime",
        price: "$30",
        tag: "Spray",
        type: "Active"
    }, {
        image: "/Sprays/Breez-Sprays-Cinnamon1-1.png",
        name: "Cinnamon 1:1",
        price: "$30",
        tag: "Spray",
        type: "Active"
    }, {
        image: "/Sprays/Breez-Sprays-CitrusRecovery.png",
        name: "Citrus Recovery",
        price: "$30",
        tag: "Spray",
        type: "Active"
    }, {
        image: "/Sprays/Breez-Sprays-OriginalMint.png",
        name: "Original Mint",
        price: "$30",
        tag: "Spray",
        type: "Active"
    }, {
        image: "/Sprays/Breez-Sprays-RoyalMint-Hybrid.png",
        name: "Royal Mint",
        price: "$30",
        tag: "Spray",
        type: "Hybrid"
    }, {
        image: "/Sprays/Breez-Sprays-RoyalMint-Indica.png",
        name: "Royal Mint",
        price: "$30",
        tag: "Spray",
        type: "Indica"
    }, {
        image: "/Sprays/Breez-Sprays-RoyalMint-Sativa.png",
        name: "Royal Mint",
        price: "$30",
        tag: "Spray",
        type: "Sativa"
    }],
    tablets: [{
        image: "/Tablets/Breez-Tablets-Hybrid.png",
        name: "Hybrid",
        price: "$30",
        tag: "Tablet",
        type: "Active"
    }, {
        image: "/Tablets/Breez-Tablets-Indica.png",
        name: "Indica",
        price: "$30",
        tag: "Tablets",
        type: "Active"
    }, {
        image: "/Tablets/Breez-Tablets-Nighttime.png",
        name: "Nighttime",
        price: "$30",
        tag: "Tablets",
        type: "Active"
    }, {
        image: "/Tablets/Breez-Tablets-Recovery-CA.png",
        name: "Recovery CA",
        price: "$30",
        tag: "Tablets",
        type: "Active"
    }, {
        image: "/Tablets/Breez-Tablets-Recovery.png",
        name: "Recovery",
        price: "$30",
        tag: "Tablets",
        type: "Active"
    }, {
        image: "/Tablets/Breez-Tablets-Relief1-1.png",
        name: "Relief 1:1",
        price: "$30",
        tag: "Tablets",
        type: "Hybrid"
    }, {
        image: "/Tablets/Breez-Tablets-Sativa.png",
        name: "Sativa",
        price: "$30",
        tag: "Tablets",
        type: "Indica"
    }],
    initSwiper() {
        const e = document.querySelectorAll(".swiper-button-next"),
            t = document.querySelectorAll(".swiper-button-prev");
        document.querySelectorAll(".c-swiper").forEach((s, a) => {
            s.children[0].children.length, this.swiper = new p(s, {
                modules: [d, m],
                grabCursor: !0,
                preloadImages: !0,
                preventClicks: !0,
                preventClicksPropagation: !0,
                slidesPerView: "auto",
                noSwiping: !0,
                noSwipingClass: "swiper-no-swiping",
                navigation: {
                    nextEl: e[a],
                    prevEl: t[a]
                },
                breakpoints: {
                    320: {
                        slidesPerGroupAuto: !1
                    },
                    766: {
                        slidesPerGroupAuto: !0,
                        slidesPerGroup: 1
                    }
                }
            })
        })
    },
    init() {
        this.initSwiper()
    }
});
n.registerPlugin(l);
var $ = () => ({
        name: "accordion",
        open: null,
        toggle(e) {
            this.open === e ? this.open = "" : this.open = e
        },
        init() {}
    }),
    b = (e = null) => ({
        name: "Quantity selector",
        quantity: e,
        increase() {
            this.$nextTick(() => {
                this.quantity++
            })
        },
        decrease() {
            this.$nextTick(() => {
                this.quantity > 0 && this.quantity--
            })
        },
        init() {}
    });
p.use([m]);
var z = () => ({
    name: "singleProductImages",
    init() {
        new p(this.$refs.slider, {
            grabCursor: !0,
            preloadImages: !0,
            preventClicks: !0,
            preventClicksPropagation: !0,
            slidesPerView: "auto",
            pagination: {
                el: this.$refs.navigation,
                clickable: !0
            }
        })
    }
});
n.registerPlugin(l);
var A = () => ({
    name: "formatsParallax",
    initParallax() {
        const e = document.querySelectorAll(".formats__category");
        l.matchMedia({
            "(min-width:821px)": function() {
                e.forEach(t => {
                    const i = t.querySelector(".formats__category-top"),
                        s = t.querySelector(".formats__category-bottom");
                    n.to(s, {
                        yPercent: 10,
                        ease: "none",
                        scrollTrigger: {
                            trigger: t,
                            scrub: !0
                        }
                    }), n.to(i, {
                        yPercent: 30,
                        ease: "none",
                        scrollTrigger: {
                            trigger: t,
                            scrub: !0
                        }
                    })
                })
            }
        })
    },
    init() {
        this.initParallax()
    }
});
n.registerPlugin(l);
var C = () => ({
        name: "Store Locator",
        zipcode: "",
        radius: 10,
        init() {
            const e = window.location.search,
                t = new URLSearchParams(e);
            t.has("zipcode") ? this.zipcode = t.get("zipcode") : this.$nextTick(() => Alpine.store("global").toggleFindModal())
        }
    }),
    R = () => ({
        name: "mapMarkers",
        map: null,
        initMap() {
            this.map = new google.maps.Map(document.getElementById("map"), {
                center: new google.maps.LatLng(34.05188515776426, -118.24742997934405),
                zoom: 12,
                mapTypeControl: !1,
                scaleControl: !0,
                streetViewControl: !1,
                rotateControl: !1,
                fullscreenControl: !0
            });
            const e = {
                    store: {
                        icon: "/map/store-icon.png"
                    },
                    delivery: {
                        icon: "/map/delivery-icon.png"
                    }
                },
                t = [{
                    position: new google.maps.LatLng(34.05557798426999, -118.24580938320099),
                    type: "store"
                }, {
                    position: new google.maps.LatLng(34.07513665089557, -118.24569751811033),
                    type: "delivery"
                }, {
                    position: new google.maps.LatLng(34.06066135449151, -118.26090537671223),
                    type: "store"
                }, {
                    position: new google.maps.LatLng(34.04007525646737, -118.23221572292742),
                    type: "delivery"
                }];
            for (let i = 0; i < t.length; i++) new google.maps.Marker({
                position: t[i].position,
                icon: e[t[i].type].icon,
                map: this.map
            })
        },
        init() {}
    }),
    P = () => ({
        name: "Find Popup",
        open: !1,
        step: 1,
        zipcode: "",
        close() {
            sessionStorage.ageGate && (Alpine.store("global").findModal = !1)
        },
        submit() {
            let e = this.zipcode.replace(/\s/g, "");
            window.location.href = "/stores.html?zipcode=" + e
        },
        init() {
            this.$watch("$store.global.findModal", e => {
                this.open = e, e === !0 ? c(this.$el) : g(this.$el)
            })
        }
    });
n.registerPlugin(l);
var k = () => ({
        name: "menu",
        open: !1,
        links: [],
        toggleMenu() {
            this.open = !this.open;
            const e = document.querySelector(".c-menu");
            this.open ? (this.linkReveal(), c(e)) : g(e)
        },
        linkEnter() {
            const e = this.$el.getAttribute("data-image");
            this.$refs.menuImage.src = e
        },
        linkReveal() {
            this.links = this.$refs.nav.querySelectorAll("li"), n.fromTo(this.links, {
                opacity: 0,
                y: 40
            }, {
                duration: .4,
                opacity: 1,
                stagger: .1,
                y: 0
            })
        },
        init() {}
    }),
    T = () => ({
        name: "filterDropdown",
        open: !1,
        options: [],
        selected: null,
        text: null,
        change(e) {
            this.open = !1, this.selected = e.value, this.text = e.text
        },
        init() {
            const e = this.$refs.select.options;
            for (let t = 0; t < e.length; t++)
                if (e[t].disabled === !0) this.text = e[t].text;
                else {
                    const i = {
                        value: e[t].value,
                        text: e[t].text
                    };
                    this.options.push(i)
                }
        }
    }),
    L = () => ({
        name: "hpGallery",
        swiper: null,
        initSwiper() {
            this.swiper = new p(".g-swiper", {
                preloadImages: !0,
                preventClicks: !0,
                preventClicksPropagation: !0,
                spaceBetween: 40,
                centeredSlides: !0,
                slidesPerView: "auto",
                loop: !0,
                loadPrevNextAmount: 3,
                allowTouchMove: !1
            })
        },
        init() {
            this.initSwiper()
        }
    });
n.registerPlugin(l);
var q = () => ({
    name: "testimonialSwiper",
    swiper: null,
    initSwiper() {
        const e = document.querySelector(".swiper-button-next"),
            t = document.querySelector(".swiper-button-prev");
        document.querySelector(".c-testimonial-swiper"), this.swiper = new p(this.$root.querySelector(".c-testimonial-swiper"), {
            modules: [d, m],
            grabCursor: !0,
            preloadImages: !0,
            preventClicks: !0,
            preventClicksPropagation: !0,
            slidesPerView: "auto",
            navigation: {
                nextEl: e,
                prevEl: t
            },
            breakpoints: {
                320: {
                    slidesPerGroupAuto: !1
                },
                766: {
                    slidesPerGroupAuto: !0,
                    slidesPerGroup: 1
                }
            }
        })
    },
    init() {
        this.initSwiper()
    }
});
n.registerPlugin(l);
window.gsap = n;
r.data("ageGate", h);
r.data("productSwiper", S);
r.data("flavoursGrid", f);
r.data("categoryGrid", w);
r.data("productCollectionSwiper", B);
r.data("accordion", $);
r.data("quantity", b);
r.data("singleProductImages", z);
r.data("formatsParallax", A);
r.data("storeLocator", C);
r.data("mapMarkers", R);
r.data("findPopup", P);
r.data("menu", k);
r.data("filterDropdown", T);
r.data("hpGallery", L);
r.data("testimonialSwiper", q);
window.Alpine = r;
r.store("global", {
    menu: !1,
    findModal: !1,
    filterModal: !1,
    navLink: !1,
    toggleFindModal() {
        this.findModal = !this.findModal
    },
    toggleFilterModal() {
        this.filterModal = !this.filterModal;
        const e = document.querySelector(".c-filter-modal");
        this.filterModal ? c(e) : g(e)
    },
    activeNavLink() {
        const e = document.querySelectorAll(".c-secondary-nav a");
        this.navLink = !this.navLink, e.forEach(t => {
            this.navLink && t.classList.add(".is-active")
        })
    },
    init() {
        console.log("Breez \u{1F525}")
    }
});
r.plugin(u);
r.plugin(M);
r.start();