<?php
   /**
   * Template Name: Shop
   *
   * @package WordPress
   * @subpackage Twenty_Fourteen
   * @since Twenty Fourteen 1.0
   */
   
   get_header();
   ?>
   <section x-data class="products-hero">
    <div class="container">
      <div class="products-hero__inner">
        <div class="cols-container items-center md:px-1-cols lg:px-0">
          <article class="w-6-cols md:w-8-cols lg:w-5-cols">
            <h1>Our Products</h1>
            <p class="max-w-md lg:pb-0">
              All our products are made using the highest quality full-spectrum
              extracts to preserve the full natural goodness of the cannabis
              plant. Take your pick.
            </p>

            <!-- Filter button -->
            <button
            class="fake-input-btn lg:hidden"
            @click="$store.global.toggleFilterModal()"
            >
            Filter by

            <div class="icon">
              <div>
                <include src="partials/svg/cart/_add-icon.svg"></include>
              </div>
            </div>
          </button>
        </article>

        <article class="hidden lg:block w-7-cols">
          <div class="products-hero__image">
            <img src="<?php echo get_template_directory_uri(); ?>/public/images/contact/lifestyle-contact.jpg" alt=""/>
          </div>
        </article>
      </div>
    </div>
  </div>
</section>

<div class="container lg:py-10">
  <section class="c-product-filter hidden lg:block">
    <div class="c-product-filter__inner">
      <div class="">
        <p class="bp-0 f-body">Shop by:</p>
      </div>

      <form action="" class="flex space-x-8">
        <!-- Flavours dropdown -->        

        <div class="w-4-cols">
         <div class="cselect">
          <select>
          <option>Flavor</option>
          <option>Berry Daytime</option>
          <option>Berry Nighttime</option>
          <option>Cinnamon</option>
          <option>Citrus</option>
          <option>Mint</option>
          <option>Unflavored</option>
        </select>
      </div>
      </div>

       <div class="w-4-cols">
         <div class="cselect">
          <select><i class="fas fa-angle-up"></i>
          <option>Flavor</option>
          <option>Berry Daytime</option>
          <option>Berry Nighttime</option>
          <option>Cinnamon</option>
          <option>Citrus</option>
          <option>Mint</option>
          <option>Unflavored</option>
        </select>
      </div>
      </div>

        <div class="w-4-cols">
         <div class="cselect">
          <select><i class="fas fa-angle-up"></i>
          <option>Flavor</option>
          <option>Berry Daytime</option>
          <option>Berry Nighttime</option>
          <option>Cinnamon</option>
          <option>Citrus</option>
          <option>Mint</option>
          <option>Unflavored</option>
        </select>
      </div>
      </div>
  </form>
</div>
</section>
</div>


<!-- Start Mints Products Area -->
<section class="projects-section c-products-carousel overflow-hidden owl-carousel">
  <div class="container">
    <div class="c-products-carousel__header">
      <a href="#" class="c-products-carousel__title"> Mints </a>

      <article class="c-products-carousel__icon">
       <img src="<?php echo get_template_directory_uri(); ?>/public/images/_mint-icon.png" />
     </article>
   </div>
   <div id="projects-carousel" class="projects-carousel box-shadow owl-carousel">
    <div class="project-item">
     <div class="post-slide">
      <div class="">
        <article>
          <a href="/single-product.html" class="product-image" style="--aspect-ratio: (1 / 1)"
          >
          <img src="http://localhost/findbreeze/wp-content/uploads/2022/04/Breez-Mints-BerryDaytime.png" :alt="mints.name" />
        </a>
      </article>

      <article class="product-detail">
        <h4 class="mints-name">Berry Daytime</h4>
        <a href="/category.html" class="product__type">
          <span x-text="mints.tag">Mint</span>
        </a>
        <p class="mints-price">$30</p>

        <div>
          <span class="c-product-tag" x-text="mints.type">Active</span>
        </div>
      </article>

      <!-- Add to cart button  -->
      <button type="button" x-show="step === 0" @click="step = 1" class="btn-primary swiper-no-swiping">
        Add to Cart
      </button>
    </div>
  </div>
</div>

<div class="project-item">
 <div class="post-slide">
  <div class="">
    <article>
      <a href="/single-product.html" class="product-image" style="--aspect-ratio: (1 / 1)"
      >
      <img src="http://localhost/findbreeze/wp-content/uploads/2022/04/Breez-Mints-BerryDaytime.png" :alt="mints.name" />
    </a>
  </article>

  <article class="product-detail">
    <h4 class="mints-name">Berry Daytime</h4>
    <a href="/category.html" class="product__type">
      <span x-text="mints.tag">Mint</span>
    </a>
    <p class="mints-price">$30</p>

    <div>
      <span class="c-product-tag" x-text="mints.type">Active</span>
    </div>
  </article>

  <!-- Add to cart button  -->
  <button type="button" x-show="step === 0" @click="step = 1" class="btn-primary swiper-no-swiping">
    Add to Cart
  </button>
</div>
</div>
</div>

<div class="project-item">
 <div class="post-slide">
  <div class="">
    <article>
      <a href="/single-product.html" class="product-image" style="--aspect-ratio: (1 / 1)"
      >
      <img src="http://localhost/findbreeze/wp-content/uploads/2022/04/Breez-Mints-BerryDaytime.png" :alt="mints.name" />
    </a>
  </article>

  <article class="product-detail">
    <h4 class="mints-name">Berry Daytime</h4>
    <a href="/category.html" class="product__type">
      <span x-text="mints.tag">Mint</span>
    </a>
    <p class="mints-price">$30</p>

    <div>
      <span class="c-product-tag" x-text="mints.type">Active</span>
    </div>
  </article>

  <!-- Add to cart button  -->
  <button type="button" x-show="step === 0" @click="step = 1" class="btn-primary swiper-no-swiping">
    Add to Cart
  </button>
</div>
</div>
</div>

<div class="project-item">
 <div class="post-slide">
  <div class="">
    <article>
      <a href="/single-product.html" class="product-image" style="--aspect-ratio: (1 / 1)"
      >
      <img src="http://localhost/findbreeze/wp-content/uploads/2022/04/Breez-Mints-BerryDaytime.png" :alt="mints.name" />
    </a>
  </article>

  <article class="product-detail">
    <h4 class="mints-name">Berry Daytime</h4>
    <a href="/category.html" class="product__type">
      <span x-text="mints.tag">Mint</span>
    </a>
    <p class="mints-price">$30</p>

    <div>
      <span class="c-product-tag" x-text="mints.type">Active</span>
    </div>
  </article>

  <!-- Add to cart button  -->
  <button type="button" x-show="step === 0" @click="step = 1" class="btn-primary swiper-no-swiping">
    Add to Cart
  </button>
</div>
</div>
</div>

</div>
</div>
</section>
<!-- End Mints Products Area -->

<!-- Start Tincture Sprays Area -->
<section class="tincture-sprays-section c-products-carousel overflow-hidden owl-carousel">
  <div class="container">
    <div class="c-products-carousel__header">
      <a href="#" class="c-products-carousel__title"> Tincture Sprays </a>

      <article class="c-products-carousel__icon">
       <img src="<?php echo get_template_directory_uri(); ?>/public/images/_spray-icon.png" />
     </article>
   </div>
   <div id="tincture-sprays-carousel" class="tincture-sprays-carousel box-shadow owl-carousel">
    <div class="project-item">
     <div class="post-slide">
      <div class="">
        <article>
          <a href="/single-product.html" class="product-image" style="--aspect-ratio: (1 / 1)"
          >
          <img src="http://localhost/findbreeze/wp-content/uploads/2022/04/Breez-Mints-BerryDaytime.png" :alt="mints.name" />
        </a>
      </article>

      <article class="product-detail">
        <h4 class="mints-name">Berry Daytime</h4>
        <a href="/category.html" class="product__type">
          <span x-text="mints.tag">Mint</span>
        </a>
        <p class="mints-price">$30</p>

        <div>
          <span class="c-product-tag" x-text="mints.type">Active</span>
        </div>
      </article>

      <!-- Add to cart button  -->
      <button type="button" x-show="step === 0" @click="step = 1" class="btn-primary swiper-no-swiping">
        Add to Cart
      </button>
    </div>
  </div>
</div>

<div class="project-item">
 <div class="post-slide">
  <div class="">
    <article>
      <a href="/single-product.html" class="product-image" style="--aspect-ratio: (1 / 1)"
      >
      <img src="http://localhost/findbreeze/wp-content/uploads/2022/04/Breez-Mints-BerryDaytime.png" :alt="mints.name" />
    </a>
  </article>

  <article class="product-detail">
    <h4 class="mints-name">Berry Daytime</h4>
    <a href="/category.html" class="product__type">
      <span x-text="mints.tag">Mint</span>
    </a>
    <p class="mints-price">$30</p>

    <div>
      <span class="c-product-tag" x-text="mints.type">Active</span>
    </div>
  </article>

  <!-- Add to cart button  -->
  <button type="button" x-show="step === 0" @click="step = 1" class="btn-primary swiper-no-swiping">
    Add to Cart
  </button>
</div>
</div>
</div>

<div class="project-item">
 <div class="post-slide">
  <div class="">
    <article>
      <a href="/single-product.html" class="product-image" style="--aspect-ratio: (1 / 1)"
      >
      <img src="http://localhost/findbreeze/wp-content/uploads/2022/04/Breez-Mints-BerryDaytime.png" :alt="mints.name" />
    </a>
  </article>

  <article class="product-detail">
    <h4 class="mints-name">Berry Daytime</h4>
    <a href="/category.html" class="product__type">
      <span x-text="mints.tag">Mint</span>
    </a>
    <p class="mints-price">$30</p>

    <div>
      <span class="c-product-tag" x-text="mints.type">Active</span>
    </div>
  </article>

  <!-- Add to cart button  -->
  <button type="button" x-show="step === 0" @click="step = 1" class="btn-primary swiper-no-swiping">
    Add to Cart
  </button>
</div>
</div>
</div>

<div class="project-item">
 <div class="post-slide">
  <div class="">
    <article>
      <a href="/single-product.html" class="product-image" style="--aspect-ratio: (1 / 1)"
      >
      <img src="http://localhost/findbreeze/wp-content/uploads/2022/04/Breez-Mints-BerryDaytime.png" :alt="mints.name" />
    </a>
  </article>

  <article class="product-detail">
    <h4 class="mints-name">Berry Daytime</h4>
    <a href="/category.html" class="product__type">
      <span x-text="mints.tag">Mint</span>
    </a>
    <p class="mints-price">$30</p>

    <div>
      <span class="c-product-tag" x-text="mints.type">Active</span>
    </div>
  </article>

  <!-- Add to cart button  -->
  <button type="button" x-show="step === 0" @click="step = 1" class="btn-primary swiper-no-swiping">
    Add to Cart
  </button>
</div>
</div>
</div>

</div>
</div>
</section>
<!-- End Tincture Sprays Products Area -->

<!-- Start Tablets Area -->
<section class="tablets-section c-products-carousel overflow-hidden owl-carousel">
  <div class="container">
    <div class="c-products-carousel__header">
      <a href="#" class="c-products-carousel__title"> Tablets </a>

      <article class="c-products-carousel__icon">
       <img src="<?php echo get_template_directory_uri(); ?>/public/images/_tablet-icon.png" />
     </article>
   </div>
   <div id="tablets-carousel" class="tincture-sprays-carousel box-shadow owl-carousel">
    <div class="project-item">
     <div class="post-slide">
      <div class="">
        <article>
          <a href="/single-product.html" class="product-image" style="--aspect-ratio: (1 / 1)"
          >
          <img src="http://localhost/findbreeze/wp-content/uploads/2022/04/Breez-Mints-BerryDaytime.png" :alt="mints.name" />
        </a>
      </article>

      <article class="product-detail">
        <h4 class="mints-name">Berry Daytime</h4>
        <a href="/category.html" class="product__type">
          <span x-text="mints.tag">Mint</span>
        </a>
        <p class="mints-price">$30</p>

        <div>
          <span class="c-product-tag" x-text="mints.type">Active</span>
        </div>
      </article>

      <!-- Add to cart button  -->
      <button type="button" x-show="step === 0" @click="step = 1" class="btn-primary swiper-no-swiping">
        Add to Cart
      </button>
    </div>
  </div>
</div>

<div class="project-item">
 <div class="post-slide">
  <div class="">
    <article>
      <a href="/single-product.html" class="product-image" style="--aspect-ratio: (1 / 1)"
      >
      <img src="http://localhost/findbreeze/wp-content/uploads/2022/04/Breez-Mints-BerryDaytime.png" :alt="mints.name" />
    </a>
  </article>

  <article class="product-detail">
    <h4 class="mints-name">Berry Daytime</h4>
    <a href="/category.html" class="product__type">
      <span x-text="mints.tag">Mint</span>
    </a>
    <p class="mints-price">$30</p>

    <div>
      <span class="c-product-tag" x-text="mints.type">Active</span>
    </div>
  </article>

  <!-- Add to cart button  -->
  <button type="button" x-show="step === 0" @click="step = 1" class="btn-primary swiper-no-swiping">
    Add to Cart
  </button>
</div>
</div>
</div>

<div class="project-item">
 <div class="post-slide">
  <div class="">
    <article>
      <a href="/single-product.html" class="product-image" style="--aspect-ratio: (1 / 1)"
      >
      <img src="http://localhost/findbreeze/wp-content/uploads/2022/04/Breez-Mints-BerryDaytime.png" :alt="mints.name" />
    </a>
  </article>

  <article class="product-detail">
    <h4 class="mints-name">Berry Daytime</h4>
    <a href="/category.html" class="product__type">
      <span x-text="mints.tag">Mint</span>
    </a>
    <p class="mints-price">$30</p>

    <div>
      <span class="c-product-tag" x-text="mints.type">Active</span>
    </div>
  </article>

  <!-- Add to cart button  -->
  <button type="button" x-show="step === 0" @click="step = 1" class="btn-primary swiper-no-swiping">
    Add to Cart
  </button>
</div>
</div>
</div>

<div class="project-item">
 <div class="post-slide">
  <div class="">
    <article>
      <a href="/single-product.html" class="product-image" style="--aspect-ratio: (1 / 1)"
      >
      <img src="http://localhost/findbreeze/wp-content/uploads/2022/04/Breez-Mints-BerryDaytime.png" :alt="mints.name" />
    </a>
  </article>

  <article class="product-detail">
    <h4 class="mints-name">Berry Daytime</h4>
    <a href="/category.html" class="product__type">
      <span x-text="mints.tag">Mint</span>
    </a>
    <p class="mints-price">$30</p>

    <div>
      <span class="c-product-tag" x-text="mints.type">Active</span>
    </div>
  </article>

  <!-- Add to cart button  -->
  <button type="button" x-show="step === 0" @click="step = 1" class="btn-primary swiper-no-swiping">
    Add to Cart
  </button>
</div>
</div>
</div>

</div>
</div>
</section>
<!-- End Tincture Sprays Products Area -->


<?php
get_footer();