<!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

<head>

    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>


    <header>
        <div x-data="menu">
            <!-- Header section -->

            <div class="container">
                <div class="c-header__inner">
                    <!-- Menu icon -->
                    <button onclick="openMenu()" class="c-header__menu">
                        <div></div>
                        <div></div>
                        <div></div>
                    </button>

                    <!-- site link -->
                    <div class="c-header__logo">
                        <a href="">
                           <?php
                           if ( function_exists( 'the_custom_logo' ) ) {
                            the_custom_logo();
                        }
                        ?>  
                    </a>
                </div>

                <div class="flex items-center space-x-4">
                    <!-- Location -->
                    <article class="c-header__location hidden md:block">
                        <div class="flex mr-6 space-x-1">
                            <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M18.5674 17.3359L14.3244 21.5789C14.1388 21.7646 13.9185 21.912 13.676 22.0125C13.4334 22.1131 13.1735 22.1648 12.9109 22.1648C12.6484 22.1648 12.3884 22.1131 12.1458 22.0125C11.9033 21.912 11.683 21.7646 11.4974 21.5789L7.2534 17.3359C6.13463 16.2171 5.37274 14.7916 5.06409 13.2398C4.75544 11.6879 4.91389 10.0794 5.51941 8.61764C6.12492 7.15585 7.1503 5.90645 8.46589 5.02741C9.78147 4.14838 11.3282 3.6792 12.9104 3.6792C14.4926 3.6792 16.0393 4.14838 17.3549 5.02741C18.6705 5.90645 19.6959 7.15585 20.3014 8.61764C20.9069 10.0794 21.0654 11.6879 20.7567 13.2398C20.4481 14.7916 19.6862 16.2171 18.5674 17.3359V17.3359Z" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M15.9104 11.679C15.9104 12.4746 15.5943 13.2377 15.0317 13.8003C14.4691 14.3629 13.706 14.679 12.9104 14.679C12.1148 14.679 11.3517 14.3629 10.7891 13.8003C10.2265 13.2377 9.9104 12.4746 9.9104 11.679C9.9104 10.8833 10.2265 10.1202 10.7891 9.55763C11.3517 8.99503 12.1148 8.67896 12.9104 8.67896C13.706 8.67896 14.4691 8.99503 15.0317 9.55763C15.5943 10.1202 15.9104 10.8833 15.9104 11.679V11.679Z" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg>

                            <span class="f-display-xsm mb-0">Los Angeles</span>
                        </div>
                    </article>

                    <!-- Search icon -->
                     <article class="z-50 lg:xl:z-0">
                        <button class="search-icon">
                                <img src="<?php echo get_template_directory_uri()?>/public/images/_search-icon.png ">
                                <div class="togglesearch">
                                    <input type="text" placeholder="Search..."/>
                                    <input type="button" class="btn-primary" value="Search"/>
                                </div>
                        </button>
                    </article>

                    <!-- cart icon -->
                    <article class="c-header__cart">
                        <a href="<?php echo get_site_url(); ?>/cart">
                            <article class="c-header__cart-mobile">
                                
                                  <?php if ( WC()->cart->get_cart_contents_count() == 0 ) : ?>
                                   <img src="<?php echo get_template_directory_uri();?>/public/images/cart.png ">
                              <?php 
                              else : ?>
                                 <img src="<?php echo get_template_directory_uri();?>/public/images/cart-dot.png ">
                              <?php endif; ?>

                            </article>
                        </a>
                    </article>
                </div>
            </div>
        </div>

        <!-- Site full screen menu section  -->

        <div class="c-menu" x-show="open" x-transition:enter="transition ease-out duration-200" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="transition ease-in duration-100" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" x-cloak="">
            <div class="c-menu__inner">
                <!-- Close menu -->
                <button onclick="closeMenu()" class="c-menu__close">
                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.88086 20.3691L20.6191 1.63089" stroke="black" stroke-width="2" stroke-linecap="round"></path>
                        <path d="M1.88086 2.13086L20.6191 20.8691" stroke="black" stroke-width="2" stroke-linecap="round"></path>
                    </svg>

                    <div class="f-body ml-6 hidden lg:block">Close</div>
                </button>

                <!-- Menu nav -->
                <article class="c-menu__nav-wrapper">
                    <nav class="container pt-40 lg:pt-0">
                        <?php //wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'c-menu__nav space-y-5 lg:text-center' ) ); ?> 
                             <ul class="c-menu__nav space-y-5 lg:text-center dropdown_menu-1" x-ref="nav">
                                 <?php

                                    // Check rows exists.
                                    $count = 1;
                                    $rows = get_field('menu_item', 'option');
                                    // print_r($rows);
                                    if( $rows ):
                                    
                                        // Loop through rows.
                                        foreach( $rows as $row ) {?>
                                            <li class="dropdown_item-<?php echo $count ?>">
                                                <a href="<?php echo $row['menu_url']; ?>" data-image="<?php echo $row['thumnail']; ?>"><?php echo $row['menu_name']; ?></a>
                                            </li>
                                            <?php 
											$count++; 
                                        // End loop.
                                        }
                                    
                                    // No value.
                                    else :
                                        // Do something...
                                    endif; ?>
                            </ul> 
                        </nav>
                    </article>

                    <!-- menu image on link hover -->
                    <article class="c-menu__image">
                        <div class="image-inside">
                            <img x-ref="menuImage" class="menu-image" src="<?php echo get_template_directory_uri();?>/images/contact/f7a7fc225c1f8939ad862e24380f36bc.jpg" alt="">
                        </div>
                    </article>
                </div>
            </div>
        </div>

    </header>

    <!-- Age gate modal -->
    <div class="c-modal" x-data="ageGate" x-show="open" x-transition:enter="transition ease-out duration-300" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="transition ease-in duration-300 delay-300" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" x-cloak="">
        <div class="container">
            <article class="c-modal__wrapper" x-show="open" x-transition:leave="transition ease-in duration-200 delay-200 transform" x-transition:leave-start="opacity-100 translate-y-0" x-transition:leave-end="opacity-0 translate-y-1">
                <div class="c-modal__inner space-y-6 text-center">
                    <div class="c-modal__logo">
                        <svg width="107" height="116" viewBox="0 0 107 116" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M81.0017 57.6496C88.3825 56.9047 95.1942 53.3504 100.027 47.7226C104.859 42.0947 107.342 34.8243 106.962 27.4164C106.582 20.0086 103.367 13.0306 97.9837 7.92707C92.6004 2.82356 85.4605 -0.0146405 78.0422 5.6797e-05H18.2683V115.25H78.0422C85.44 115.235 92.5502 112.383 97.9083 107.283C103.266 102.183 106.464 95.2218 106.844 87.8344C107.223 80.447 104.755 73.1954 99.9472 67.5731C95.1397 61.9509 88.359 58.3861 81.0017 57.6131V57.6496Z" fill="#EBCC86"></path>
                            <path d="M12.9583 25.8899C14.1195 26.1389 15.1668 26.7623 15.9391 27.6644C16.7115 28.5664 17.1661 29.6972 17.2331 30.8828C17.2331 34.5361 14.0544 36.9717 9.38992 36.9717H0V15.6362H8.52521C12.9827 15.6362 16.3441 17.8891 16.3441 21.579C16.2871 22.5494 15.9345 23.479 15.3336 24.2432C14.7327 25.0073 13.9123 25.5692 12.9827 25.8534L12.9583 25.8899ZM7.70923 24.3799C10.2668 24.3799 10.9123 23.2595 10.9123 22.0417C10.9123 20.6291 9.6944 19.935 8.25729 19.935H5.41961V24.3799H7.70923ZM5.41961 28.0332V32.6729H9.21941C10.8027 32.6729 11.7526 31.8083 11.7526 30.5905C11.7526 29.3727 10.9245 28.0332 7.81885 28.0332H5.41961Z" fill="#2D2A26"></path>
                            <path d="M25.6243 24.5991C25.8044 23.5624 26.3436 22.6222 27.1475 21.9431C27.9513 21.264 28.9685 20.8894 30.0208 20.8849C30.3762 20.8719 30.7318 20.9005 31.0804 20.9701V26.097C30.4291 25.9505 29.7628 25.881 29.0953 25.89C28.66 25.8562 28.2226 25.916 27.8124 26.0654C27.4023 26.2147 27.0289 26.4502 26.7173 26.7559C26.4058 27.0617 26.1633 27.4306 26.0063 27.8379C25.8493 28.2452 25.7813 28.6813 25.807 29.1171V37.0204H20.5579V21.1893H25.6243V24.5991Z" fill="#2D2A26"></path>
                            <path d="M32.6274 29.1778C32.6274 24.6599 36.0132 20.8848 41.445 20.8848C46.2312 20.8848 49.8605 23.844 49.8605 29.4092C49.8728 29.6323 49.8728 29.8559 49.8605 30.079H37.6817C37.7426 31.516 39.3867 33.1356 42.3827 33.1356C43.9838 33.1146 45.5218 32.5082 46.7062 31.4307C47.1447 31.8448 48.8741 34.0124 49.3003 34.4143C48.3442 35.3921 47.1929 36.1577 45.9213 36.6613C44.6497 37.1648 43.2864 37.395 41.9199 37.3369C36.0619 37.3369 32.6518 33.6836 32.6518 29.1657L32.6274 29.1778ZM37.7426 27.2781H44.9403C44.7576 25.4758 42.9917 24.7695 41.4328 24.7695C39.8739 24.7695 38.047 25.4514 37.7791 27.2781" fill="#2D2A26"></path>
                            <path d="M51.9062 29.1778C51.9062 24.6599 55.292 20.8848 60.7237 20.8848C65.51 20.8848 69.1393 23.844 69.1393 29.4092C69.1516 29.6323 69.1516 29.8559 69.1393 30.079H56.9605C56.9605 31.516 58.6655 33.1356 61.6615 33.1356C63.2659 33.1131 64.8072 32.507 65.9972 31.4307C66.4235 31.8448 68.1529 34.0124 68.5913 34.4143C67.6338 35.3937 66.4805 36.1602 65.2067 36.6638C63.9329 37.1674 62.5672 37.3968 61.1987 37.3369C55.3407 37.3369 51.9306 33.6836 51.9306 29.1657L51.9062 29.1778ZM57.0214 27.2781H64.2191C64.0364 25.4758 62.2705 24.7695 60.7116 24.7695C59.1527 24.7695 57.3258 25.4514 57.0579 27.2781" fill="#2D2A26"></path>
                            <path d="M87.2736 37.0084H71.0757V34.0492L79.2477 25.3056H71.4045V21.2139H86.8108V24.234L78.6753 32.9289H87.2736V37.0084Z" fill="#2D2A26"></path>
                            <path d="M89.6244 21.9445V24.7576H88.7719V21.9445H87.7732V21.2139H90.6352V21.9445H89.6244Z" fill="#2D2A26"></path>
                            <path d="M94.2401 24.7576L94.2523 22.249H94.2401L93.3267 24.7576H92.7178L91.8287 22.249H91.8044L91.8287 24.7576H91.0249V21.2139H92.2428L93.0466 23.4911H93.071L93.8382 21.2139H95.0805V24.7576H94.2401Z" fill="#2D2A26"></path>
                        </svg>

                    </div>

                    <div>
                        <h3>Are you over 21?</h3>
                    </div>

                    <div>
                        <button type="button" onclick="close()" class="btn-primary">
                            Yes
                        </button>
                    </div>

                    <div>
                        <a href="">
                            <button class="btn-secondary bg-shade">No</button>
                        </a>
                    </div>
                </div>
            </article>
        </div>
    </div>