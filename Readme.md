## Folder Stucture

 * _ui
    * css
        * bootstrap
        * style.css
        * bootstrap.css
    * js
    * media
        * fonts
        * icomoon
        * uploads
 *  elementor
   * blocks
   * templates

## Details
### [_ui] Folder
this will host all the resources that will be called into the frontend and is further seperated into css,js and media.

#### [_ui - bootstrap] Folder
this will contain the scss files for bootstrap that will based on usage be added on the root [css] folder

### [css] Folder
contains all the css files that will be included in the theme.

### [js] Folder
contains all js libraries that will be called

### [media] Folder
contains all major media files like logo, footer logo, social images

#### [media - uploads] Folder
contains all files that were tmp used while creating the html process e.g. slide-1, slide-2 etc.

#### [media - fonts] Folder
contains all custom fonts if needed, divided into individual folders

#### [media - fonts - icomoon] Folder
contains all icons included

### [elementor] Folder
contains all elementor elements divided into blocks/templates