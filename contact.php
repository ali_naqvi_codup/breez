<?php

/**
 * Template Name: Contact Us
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();
?>

<section class="c-hero">
	<div class="lg:container">
		<div class="c-hero__inner">
			<article class="hidden lg:block lg:w-5-cols">
				<h1><?php echo get_field('contact_heading') ?></h1>
				<p class="max-w-md">
					<?php echo get_field('contact_text') ?>
				</p>
			</article>

			<article class="relative lg:w-7-cols">
				<div class="c-hero__image">
					<img src="<?php echo get_field('get_in_touch_bg_image') ?>" alt="" />
					<div class="c-hero__header">
						<?php echo get_field('get_in_touch_text') ?>
					</div>
				</div>
			</article>
		</div>
	</div>
</section>

<section class="contact-details">
	<div class="container md:px-1-cols lg:px-0">
		<div class="contact-details__inner">
			<div class="lg:flex lg:flex-col lg-col">
				<!-- Delivery Orders  -->
				<article class="contact-details__group lg:flex-1">
					<div class="group__icon">
						<img src="<?php echo get_field('delivery_orders_icon') ?>" alt="" />
					</div>

					<div class="group__details">
						<p><?php echo get_field('delivery_orders_heading') ?></p>
						<h4><?php echo get_field('delivery_orders_contact') ?></h4>
						<h4><?php echo get_field('delivery_orders_email') ?></h4>
					</div>
				</article>

				<!-- Wholesale orders -->

				<article class="contact-details__group lg:flex-1">
					<div class="group__icon">
						<img src="<?php echo get_field('wholesale_orders_icon') ?>" alt="" />
					</div>

					<div class="group__details">
						<p><?php echo get_field('wholesale_orders_heading') ?></p>
						<h4><?php echo get_field('wholesale_orders_text') ?></h4>
						<h4><?php echo get_field('wholesale_orders_email') ?></h4>
					</div>
				</article>
			</div>

			<!-- <div class="lg:w-4-cols"> -->
				<div class="lg:flex lg:flex-col lg:ml-16 lg-col">
					<!-- General enquiries -->
					<article
					class="contact-details__group lg:flex-1 lg:border-l lg:border-l-primary"
					>
					<div class="lg:ml-1/4-cols xl:ml-1/2-cols">
						<div class="group__icon">
							<img src="<?php echo get_field('general_enquiries_icon') ?>" alt="" />
						</div>

						<div class="group__details">
							<p><?php echo get_field('general_enquiries_heading') ?></p>
							<h4><?php echo get_field('general_enquiries_email') ?></h4>
						</div>
					</div>
				</article>

				<!-- Press enquiries -->
				<article
				class="contact-details__group lg:flex-1 lg:border-l lg:border-l-primary"
				>
				<div class="lg:ml-1/4-cols xl:ml-1/2-cols">
					<div class="group__icon">
						<img src="<?php echo get_field('press_enquiries_icon') ?>" alt="" />
					</div>

					<div class="group__details">
						<p><?php echo get_field('press_enquiries_heading') ?></p>
						<h4><?php echo get_field('press_enquiries_email') ?></h4>
					</div>
				</div>
			</article>
		</div>
	</div>
</div>
</section>


<?php
get_footer();
?>