<!-- Mailing list section -->
<section class="mailing-list">
  <div class="mailing-list__inner lg:container cols-container">
    <div class="mailing-list__image-top w-4-cols ml-2-cols mb-16 md:mb-20 md:ml-0 lg:mb-0 lg:ratio lg:ratio-1x1 lg:w-5-cols overflow-hidden">
      <?php
      if(is_active_sidebar('mailing-before-footer-1')){
        dynamic_sidebar('mailing-before-footer-1');
      }
      ?>
    </div>

    <div class="mailing-list__image-bottom w-4-cols -ml-2-cols mt-16 md:mt-20 lg:mt-0 md:-ml-0 lg:ratio lg:ratio-1x1 lg:w-5-cols lg:ml-2-cols">
     <?php
     if(is_active_sidebar('mailing-before-footer-2')){
      dynamic_sidebar('mailing-before-footer-2');
    }
    ?>
  </div>

  <!-- content box -->
  <article class="mailing-list__content">
    <div class="content__inner">
      <h3>Unlock your day with full-spectrum cannabis</h3>

     <!--    <form class="space-y-4 lg:px-4" action="">
            <div class="c-input">
                <input name="email" type="email" placeholder="Email Address">

                <div class="send-icon">
                    <div>
                        <svg class="alt-icon" viewBox="0 0 40 37" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="40" height="37" rx="6" fill="#EBCC86"></rect>
                            <path d="M13 19L27 19" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M20 12L27 19L20 26" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                        </svg>

                    </div>
                    <div>
                        <svg class="alt-icon" viewBox="0 0 40 37" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="40" height="37" rx="6" fill="#EBCC86"></rect>
                            <path d="M13 19L27 19" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M20 12L27 19L20 26" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                        </svg>

                    </div>
                </div>
            </div>

            <button class="btn-secondary" type="submit">Sign Up</button>
          </form> -->
          <div class="c-input">
            <?php echo do_shortcode('[gravityform id="2" title="false"]'); ?>
            <div class="send-icon">
              <div>
               <img src="<?php echo get_template_directory_uri(); ?>/public/images/_email-icon-secondary.png">
             </div>
             <div>
              <img src="<?php echo get_template_directory_uri(); ?>/public/images/_email-icon-secondary.png">
            </div>
          </div>
        </div>
      </div>
    </article>
  </div>
</section>

</main>

<footer>
  <div class="footer__inner">
    <div class="container">
      <div class="container-reset cols-container lg:pb-20">
        <article class="hidden lg:block lg:w-2-cols">
          <div class="footer__logo-lg">
           <?php
           if(is_active_sidebar('footer-sidebar-1')){
            dynamic_sidebar('footer-sidebar-1');
          }
          ?>
        </div>
      </article>

      <article class="md:w-3-cols mb-8 lg:mb-0">
        <nav>
          <?php
          if(is_active_sidebar('footer-sidebar-2')){
            dynamic_sidebar('footer-sidebar-2');
          }
          ?>
        </nav>
      </article>

      <article class="w-6-cols md:w-4-cols mb-8 lg:mb-0 xxl:w-3-cols">
        <?php
        if(is_active_sidebar('footer-sidebar-3')){
          dynamic_sidebar('footer-sidebar-3');
        }
        ?>
      </article>

      <div class="w-6-cols md:w-3-cols mb-8 lg:mb-0">
        <div class="footer__social">
          <?php
          if(is_active_sidebar('footer-sidebar-4')){
            dynamic_sidebar('footer-sidebar-4');
          }
          ?>
          <?php
          if(is_active_sidebar('footer-sidebar-5')){
            dynamic_sidebar('footer-sidebar-5');
          }
          ?>
        </div>
      </div>
    </div>

    <article class="footer__legal lg:breakout">
      <div class="w-10 lg:hidden">
        <svg viewBox="0 0 39 51" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M27.573 25.5108C30.8171 25.1812 33.811 23.6083 35.935 21.1179C38.059 18.6275 39.1504 15.4103 38.9833 12.1322C38.8161 8.85408 37.4033 5.76622 35.0371 3.50784C32.671 1.24947 29.5328 -0.00647862 26.2723 2.51335e-05H0V51H26.2723C29.5238 50.9931 32.6489 49.7312 35.004 47.4742C37.359 45.2172 38.7646 42.137 38.9313 38.868C39.0981 35.5989 38.0132 32.39 35.9001 29.9021C33.7871 27.4142 30.8068 25.8367 27.573 25.4946V25.5108Z" fill="#EBCC86"></path>
        </svg>

      </div>

      <div class="lg:flex lg:container">
        <div class="footer__company-details lg:w-5-cols">

          <?php
          if(is_active_sidebar('copyright')){
            dynamic_sidebar('copyright');
          }
          ?>
        </div>

        <div class="lg:w-7-cols">
          <?php
          if(is_active_sidebar('footer-sidebar-6')){
            dynamic_sidebar('footer-sidebar-6');
          }
          ?>
        </div>
      </div>
    </article>
  </div>
</div>

</footer>