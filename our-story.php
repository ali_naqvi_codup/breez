<?php

/**
 * Template Name: Our Story
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();
?>
<!-- Hero start-->
<section class="hero-os breakout">
  <div class="container md:px-1-cols">
    <div class="hero-os__inner pt-20 lg:pt-40">
      <h1>
       <?php echo get_field('main_heading') ?>
      </h1>

      <div class="hero-os__image">
        <img src="<?php echo get_field('main_image') ?>" alt="" />
      </div>
    </div>
  </div>
</section>

<!-- Our Story start-->
<section class="story mb-20 lg:mb-40">
  <div class="container md:px-1-cols lg:px-0">
    <div class="cols-container">
      <article
      class="text-center w-6-cols md:w-8-cols lg:w-5-cols lg:ml-1-cols lg:text-left"
      >
      <h2><?php echo get_field('our_story_heading') ?></h2>
      <p>
         <?php echo get_field('our_story_text') ?>
      </p>
      <span class="text-primary f-display-xsm"
      > <?php echo get_field('our_story_signature') ?></span
      >
    </article>

    <article class="story__icon w-6-cols mb-20 lg:mb-0">
       <img src="<?php echo get_field('our_story_image') ?>" alt="" />
    </article>
  </div>
</div>
</section>

<!-- Testimonial Section -->
<section x-data="testimonialSwiper" class="c-testimonial overflow-hidden py-20 lg:py-40">
    <div class="container md:px-1-cols lg:px-0">
        <div class="c-testimonial__header">
            <h2 class="c-testimonial__title">Hear it From Our Fans.</h2>

            <article class="c-testimonial__icon">
                <img src="<?php echo get_template_directory_uri(); ?>/public/images/testimonial_icon.png" alt="" />
            </article>
        </div>
      </div>

        <!-- Slider main container -->
        <article class="c-testimonial-swiper">
            <!-- Additional required wrapper -->
            <div id="testimonial-carousel" class="tincture-sprays-carousel box-shadow owl-carousel">
                <!-- Slides -->

                <?php
                $args = array( 'post_type' => 'testimonials', 'posts_per_page' => 4 );
                $loop = new WP_Query( $args );
                while ( $loop->have_posts() ) : $loop->the_post(); ?>

                    <div class="swiper-slide project-item">
                        <div class="swiper__inner post-slide">
                            <?php echo get_the_content(); ?>                       

                    </div>
                </div>

            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
        </div>
    </article>
</section>
<?php
get_footer();
