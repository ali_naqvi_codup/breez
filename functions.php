<?php

//Basic WP Setup
function theme_setup() {
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_post_type_support( 'page', 'excerpt' );
}
add_action( 'after_setup_theme', 'theme_setup' );

require_once ('includes/functions/security.php');
require_once ('includes/functions/boot.php');
require_once ('includes/functions/css.php');
require_once ('includes/functions/js.php');
require_once ('includes/functions/helpers.php');
require_once('includes/functions/lqip-lazyload.php');
require_once('includes/functions/findbreeze-widgets.php');
require_once('includes/functions/custom-post-type.php');
require_once('includes/functions/register-nav-menus.php');
// require_once ('includes/functions/primary-woocommerce-function.php');


//elementor
// require_once('includes/elementor/widgets/init.php');




		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 190,
				'width'       => 190,
				'flex-width'  => false,
				'flex-height' => false,
			)
		);

       



		// Theme Option
		add_action('acf/init', 'my_acf_op_init');
		function my_acf_op_init() {

    // Check function exists.
			if( function_exists('acf_add_options_page') ) {

        // Register options page.
				$option_page = acf_add_options_page(array(
					'page_title'    => __('Findbreeze Theme Option'),
					'menu_title'    => __('Findbreeze Theme Option'),
					'menu_slug'     => 'site-content-settings',
					'capability'    => 'edit_posts',
					'redirect'      => false
				));
			}
		}

		function woocommerce_support(){
			add_theme_support('woocommerce');
		}

		add_action('after_setup_theme', 'woocommerce_support');

		// Work Checkout fields placeholders
		add_filter('woocommerce_checkout_fields', 'custom_override_checkout_fields');
		function custom_override_checkout_fields($fields)
		{
			//unset($fields['billing']['billing_address_2']);
			unset($fields['billing']['billing_company']);
			$fields['billing']['billing_first_name']['placeholder'] = 'First Name'; 
			$fields['billing']['billing_last_name']['placeholder'] = 'Last Name';
			$fields['billing']['billing_email']['placeholder'] = 'Email Address ';
			$fields['billing']['billing_phone']['placeholder'] = 'Phone ';
			$fields['billing']['billing_city']['placeholder'] = 'City';
			$fields['billing']['billing_postcode']['placeholder'] = 'Zip Code';
			return $fields;
		}

		add_action("wp_ajax_nopriv_set_content", "set_content");
		add_action('wp_ajax_set_content', 'set_content');
		function set_content() {
			if(isset($_POST['item_id']) && isset($_POST['quantity']))
			{
				foreach( WC()->cart->get_cart() as $cart_item ){
					if($_POST['item_id']==$cart_item['product_id'])
					{
						echo WC()->cart->set_quantity($cart_item['key'],$_POST['quantity']);
					}
				}
			}
			die();
		}
