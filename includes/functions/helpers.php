<?php

// render_template("file", ['type' => 'edit'])
function render_template($template, $args = array()) {
	global $post;
	$template = strpos(".php", $template) ? $template : $template . ".php";
    if(locate_template($template, false, false)){
    	return include(locate_template($template, false, false));
    }else{
        echo $template." - Not Found.";
    }
}