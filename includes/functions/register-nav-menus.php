<?php
// This theme uses wp_nav_menu() in three locations.
		register_nav_menus(
			array(  
				'primary' => __( 'Main Menu', 'theme-breeze' ),  
				'footer' => __('Footer Menu', 'theme-breeze'),

			) );