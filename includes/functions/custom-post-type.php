<?php
// Our custom post type function
		function create_posttype() {

			register_post_type( 'testimonials',
    // CPT Options
				array(
					'labels' => array(
						'name' => __( 'Testimonials' ),
						'singular_name' => __( 'Testimonials' )
					),
					'public' => true,
					'has_archive' => true,
					'rewrite' => array('slug' => 'testimonials'),
					'show_in_rest' => true,
					'supports' => array( 
						'title', 
						'editor', 
						'custom-fields' 
					)

				)
			);
		}
// Hooking up our function to theme setup
		add_action( 'init', 'create_posttype' );
		
		
		
///flavor taxonomy function
function wporg_register_taxonomy_flavor() {
    $labels = array(
        'name'              => _x( 'Flavor', 'taxonomy general name' ),
        'singular_name'     => _x( 'Flavor', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Flavor' ),
        'all_items'         => __( 'All Flavor' ),
        'parent_item'       => __( 'Parent Flavor' ),
        'parent_item_colon' => __( 'Parent Flavor:' ),
        'edit_item'         => __( 'Edit Flavor' ),
        'update_item'       => __( 'Update Flavor' ),
        'add_new_item'      => __( 'Add New Flavor' ),
        'new_item_name'     => __( 'New Flavor Name' ),
        'menu_name'         => __( 'Flavor' ),
    );
    $args   = array(
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => [ 'slug' => 'Flavor' ],
    );
    register_taxonomy( 'Flavor', [ 'product' ], $args );
}
add_action( 'init', 'wporg_register_taxonomy_flavor' );


///effects taxonomy function
function wporg_register_taxonomy_effects() {
    $labels = array(
        'name'              => _x( 'Effects', 'taxonomy general name' ),
        'singular_name'     => _x( 'Effects', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Effects' ),
        'all_items'         => __( 'All Effects' ),
        'parent_item'       => __( 'Parent Effects' ),
        'parent_item_colon' => __( 'Parent Effects:' ),
        'edit_item'         => __( 'Edit Effects' ),
        'update_item'       => __( 'Update Effects' ),
        'add_new_item'      => __( 'Add New Effects' ),
        'new_item_name'     => __( 'New Effects Name' ),
        'menu_name'         => __( 'Effects' ),
    );
    $args   = array(
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => [ 'slug' => 'Effects' ],
    );
    register_taxonomy( 'Effects', [ 'product' ], $args );
}
add_action( 'init', 'wporg_register_taxonomy_effects' );


///DOSE taxonomy function
function wporg_register_taxonomy_dose() {
    $labels = array(
        'name'              => _x( 'Dose', 'taxonomy general name' ),
        'singular_name'     => _x( 'Dose', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Dose' ),
        'all_items'         => __( 'All Dose' ),
        'parent_item'       => __( 'Parent Dose' ),
        'parent_item_colon' => __( 'Parent Dose:' ),
        'edit_item'         => __( 'Edit Dose' ),
        'update_item'       => __( 'Update Dose' ),
        'add_new_item'      => __( 'Add New Dose' ),
        'new_item_name'     => __( 'New Dose Name' ),
        'menu_name'         => __( 'Dose' ),
    );
    $args   = array(
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => [ 'slug' => 'Dose' ],
    );
    register_taxonomy( 'Dose', [ 'product' ], $args );
}
add_action( 'init', 'wporg_register_taxonomy_dose' );