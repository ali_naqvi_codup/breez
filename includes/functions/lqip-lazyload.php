<?php

add_action( 'after_setup_theme', 'wp_blur_up_image_size' );
function wp_blur_up_image_size() {
    add_image_size( 'prethumb', 16, 16, false );
}

// defer lazyload script
function wpr_defer_lazyload( $return ) {
	$return = "<script type='rocketlazyloadscript' data-minify='1' data-rocket-type='text/javascript' defer src='". get_bloginfo('url') ."/wp-content/plugins/wp-rocket/assets/js/lazyload/16.1/lazyload.min.js'></script>";
    return $return;
}
add_filter( 'rocket_lazyload_script_tag', 'wpr_defer_lazyload' );

// add class to all images wp-image-{id}
function lwp_attachment_id_class($attr, $attachment) {
	$class_attr = isset($attr['class']) ? $attr['class'] : '';
	$has_class = preg_match(
		'/wp\-image\-[0-9]+/',
		$class_attr,
		$matches
	);

	// Check if the image is missing the class
	if (!$has_class) {
		$class_attr .= sprintf(' wp-image-%d', $attachment->ID);
		// Use ltrim to to remove leading space if necessary
		$attr['class'] = ltrim($class_attr);
	}
	return $attr;
}

add_filter( 'wp_get_attachment_image_attributes', 'lwp_attachment_id_class', 10, 2);

// on wprocket image generate add our known image
function wpr_modify_images_html($html) {
	$regex = '/wp-image-(\d+)/';
	preg_match($regex, $html, $match);

	if (isset($match) && !empty($match)) {
		$image_id = str_replace('wp-image-', '', $match[0]);

		$base64 = get_post_meta($image_id, "base64");

		if (isset($base64[0])) {
			$mimeType = get_post_mime_type($image_id);
			$html = preg_replace('@\ssrc\s*=\s*(\'|")(?<src>.*)\1@iUs', ' src="data:image/' . $mimeType . ';base64,' . $base64[0] . '"', $html);
		}
	}


	return $html;
}
add_filter('rocket_lazyload_html', 'wpr_modify_images_html', 99, 5);

function filter_wp_generate_attachment_metadata( $metadata, $attachment_id ) {
    // Check if the 'tiny' version exists.
    if(isset($metadata["sizes"]["prethumb"])){
        // Get the image src
        $src = wp_get_attachment_image_src($attachment_id, "prethumb");
		// base64 encode the image

		$arrContextOptions = [
			'ssl' => [
				'verify_peer' => false,
				'verify_peer_name' => false,
			],
		];

        // $base64 = base64_encode(file_get_contents($src[0], false, stream_context_create($arrContextOptions)));
        $base64 = base64_encode(file_get_contents($src[0], false));

        // Add the base64 encoded value as a custom filed to the image
        update_post_meta( $attachment_id, 'base64', $base64 );
    }
    return $metadata;
};

add_filter( 'wp_generate_attachment_metadata', 'filter_wp_generate_attachment_metadata', 10, 2 );


//get lazyload image
function get_lazy_image($id, $alt = "", $width = "", $height = "", $class = "", $custom_image = ""){
    ob_start();
        if(!empty($custom_image)) : ?>
            <img class="<?php echo $class; ?>" src="data:image/<?php echo $custom_image['type'] ?>;base64, <?php echo $custom_image['base_64'] ?>" data-lazy-src="<?php echo $custom_image['img'] ?>" alt="<?php echo $alt;?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>">
        <?php else :
            $mimeType = get_post_mime_type($id);
            $base64 = get_post_meta($id, "base64");
            $srcset = wp_get_attachment_image_srcset($id, 'full');
		    if(isset($base64[0])):?>
                <img class="<?php echo $class; ?>" src="data:image/<?php echo $mimeType ?>;base64, <?php echo $base64[0] ?>" data-lazy-src="<?php echo wp_get_attachment_image_src($id, "full")[0] ?>" alt="<?php echo $alt;?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>"  srcset="<?php echo $srcset ?>">
            <?php else:
                $src = wp_get_attachment_image_src( $id, 'full' )[0];
                $srcset = wp_get_attachment_image_srcset( $id, 'full' ); ?>
                <img  class="<?php echo $class; ?>" alt="<?php echo $alt;?>" src="<?php echo $src ?>" srcset="<?php echo $srcset ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>">
            <?php endif;
        endif;
    echo ob_get_clean();
}