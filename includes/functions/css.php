<?php
function theme_styles() {
	wp_enqueue_style( 'main_stylesheet', get_theme_file_uri( '/_ui/css/theme.css' ), array(), filemtime(get_stylesheet_directory() . '/_ui/css/theme.css') );

	wp_enqueue_style( '_entry', get_theme_file_uri( '/_ui/scss/_entry.scss' ), array(), filemtime(get_stylesheet_directory() . '/_ui/scss/_entry.scss') );

	wp_enqueue_style( 'Owl Carousel', get_theme_file_uri( '/_ui/css/owl.carousel.css' ) );
	wp_enqueue_style( 'Font Awesome Min', "https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css");
}
add_action( 'wp_enqueue_scripts', 'theme_styles' );