<?php
function theme_scripts() {
	if (!is_admin()) {
		wp_deregister_script('jquery');
	}

	wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.6.0.min.js',  true);
	wp_enqueue_script('custom', get_theme_file_uri('/_ui/js/custom.js'), array('jquery'), true);

	wp_enqueue_script('vendor', get_theme_file_uri('/_ui/js/vendor.js'), array('jquery'), filemtime(get_stylesheet_directory() . '/_ui/js/vendor.js'), true);
	wp_enqueue_script('theme', get_theme_file_uri('/_ui/js/theme.js'), array('vendor'), filemtime(get_stylesheet_directory() . '/_ui/js/theme.js'), true);

	wp_enqueue_script('Main JS', get_theme_file_uri('/_ui/js/main.js'));
	wp_enqueue_script('Owl Carousel', get_theme_file_uri('/_ui/js/owl.carousel.min.js'));
	
}

add_action('wp_enqueue_scripts', 'theme_scripts');