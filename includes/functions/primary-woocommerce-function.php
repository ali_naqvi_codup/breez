<?php
/**
 * Load WooCommerce compatibility file.
 */
// if ( class_exists( 'WooCommerce' ) ) {
// 	require get_template_directory() . '/inc/woocommerce.php';
// }
function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_filter('post_class', function($classes, $class, $product_id) {
    $classes = array_merge(['box-product'], $classes);
    return $classes;
},10,3);


 add_filter( 'woocommerce_loop_add_to_cart_link', 'quantity_inputs_for_woocommerce_loop_add_to_cart_link', 10, 2 );

 function quantity_inputs_for_woocommerce_loop_add_to_cart_link( $html, $product ) {
     if ( is_user_logged_in() && is_shop() && $product && $product->is_type( 'simple' ) && $product->is_purchasable() && $product->is_in_stock() && ! $product->is_sold_individually() ) {
         $html = '<form action="' . esc_url( $product->add_to_cart_url() ) . '" class="cart" method="post" enctype="multipart/form-data">';
         $html .= woocommerce_quantity_input( array(), $product, false );
         $html .= '<button type="submit" class="button alt">' . esc_html( $product->add_to_cart_text() ) . '</button>';
         $html .= '</form>';
     }
     return $html;
 }
 add_theme_support('woocommerce');
 add_filter( 'woocommerce_add_to_cart_fragments', 'misha_add_to_cart_fragment' );

 function misha_add_to_cart_fragment( $fragments ) {
 
     global $woocommerce;
 
     $fragments['.misha-cart'] = '<a href="' . wc_get_cart_url() . '" class="misha-cart">Cart (' . $woocommerce->cart->cart_contents_count . ')</a>';
      return $fragments;
 
  }
 
?>

