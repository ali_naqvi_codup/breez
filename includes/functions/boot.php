<?php
//allow svgs
function cc_mime_types($mimes) {
    // New allowed mime types.
    $mimes['svg'] = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';
    $mimes['doc'] = 'application/msword';

    // Optional. Remove a mime type.
    unset($mimes['exe']);

    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


//disable gutenburg
add_filter('use_block_editor_for_post', '__return_false', 10);

//Page Slug Body Class
function add_body_class($classes) {
    global $post;

    if (isset($post)) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }

    return $classes;
}
add_filter('body_class', 'add_body_class');